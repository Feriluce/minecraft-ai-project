package minecraftBot.behaviorTree;

import java.util.ArrayList;

import minecraftBot.behaviorTree.leaves.Leaf;
import minecraftBot.exceptions.NoReadyException;
import net.minecraft.entity.Entity;


/**
 *Abstract class to represent a node in the behavior tree.
 */
public abstract class Node {
	public Node parent;
	public ArrayList<Node> children;
	public Entity entity;
	public NodeStatus status;
	public NodeType type;
	
	public Node(Entity entity, NodeType type){
		this.entity = entity;
		this.status = NodeStatus.READY;
		this.type = type;
		this.children = new ArrayList<Node>();		
	}
	
	
	/**
	 * Copies a node, unless its a leaf. Returns leaves without copying.
	 */
	public static Node copyNode(Node node){
		switch(node.type){
		case SELECTOR:
			return new Selector((Selector) node);
		case SEQUENCE:
			return new Sequence((Sequence) node);
		case DECORATOR:
			return new Decorator((Decorator) node);
		case LEAF:
			return node;
		}
		return null;
	}
	
	
	/**
	 * @return Returns the result of executing a node.
	 * @throws NoReadyException 
	 */
	public abstract NodeStatus execute() throws NoReadyException;
	
}
