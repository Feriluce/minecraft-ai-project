package minecraftBot.behaviorTree;

import net.minecraft.entity.Entity;

public class TreeTest {
	 public static void main(String[] args) throws Exception{
		 Entity entity = null;
		 Node root = new Selector(entity, SelectorType.PRIORITY);
		 Node child1 = new Decorator(entity,DecoratorType.NUMITERATIONSDEC, 3);
		 Node child11 = new Selector(entity, SelectorType.RANDOM);
		 Node child2 = new Sequence(entity);
		 
		 root.children.add(child1);
		 root.children.add(child2);
		 child1.children.add(child11);
		 
		 BehaviorTree tree = new BehaviorTree(root);
		 
		 TreeWriter writer = new TreeWriter("testTree.xml");
		 TreeParser parser = new TreeParser(entity);
		 
		 writer.saveTree(tree);
		 BehaviorTree datTree = parser.parseTree("testTree.xml");
		 System.out.println("dattree");
	 }
}
