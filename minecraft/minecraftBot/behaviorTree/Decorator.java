package minecraftBot.behaviorTree;

import minecraftBot.exceptions.NoReadyException;
import net.minecraft.entity.Entity;

/**
 *Implements the decorator node of a behaviour tree.
 */
public class Decorator extends Node {
	
	public int numTrials;
	public int curTrials;
	public long startTime;
	public long timeToRun;
	public DecoratorType decType;
	/// dobbiamo controllare se ha un solo figlio o no?
	public Decorator (Entity entity, DecoratorType type, int trials) 
	{

		super(entity, NodeType.DECORATOR);
		this.decType = type;
		this.numTrials=trials;
		curTrials = 0;
	}

	public Decorator(Decorator dec){
		super(dec.entity, dec.type);
	}
	
	public Decorator(Entity entity, DecoratorType type, long timeToRun){
		super(entity, NodeType.DECORATOR);
		this.decType = type;
		switch(type){
		case TIMEDEC:
			this.startTime = System.currentTimeMillis();
			this.timeToRun  = timeToRun;
			break;
		case INVERTEDTIMEDEC:
			this.startTime = System.currentTimeMillis()+timeToRun;
			this.timeToRun = timeToRun;
			break;
		default:
			break;
		}
		
		
	}
	
	@Override
	public NodeStatus execute() throws NoReadyException
	{

		switch(decType){
		case NUMITERATIONSDEC:
			if(curTrials >= numTrials){
				this.status = NodeStatus.FAIL;
				return NodeStatus.FAIL;
			}
			
			curTrials++;
			this.status = children.get(0).execute();
			return this.status;
		case TIMEDEC:
			if(startTime+timeToRun < System.currentTimeMillis()){
				this.status = NodeStatus.FAIL;
				return this.status;
			}
			
			this.status = children.get(0).execute();
			return this.status;
		case INVERTEDTIMEDEC:
			if(System.currentTimeMillis() < startTime)
				this.status = NodeStatus.FAIL;
			else
				this.status = NodeStatus.SUCCESS;
			return this.status;
		}
		
		return NodeStatus.FAIL;
	}

}

