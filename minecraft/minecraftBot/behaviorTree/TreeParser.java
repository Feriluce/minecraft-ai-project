package minecraftBot.behaviorTree;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import minecraftBot.behaviorTree.leaves.Leaf;
import minecraftBot.behaviorTree.leaves.LeafType;
import net.minecraft.entity.Entity;

public class TreeParser {
	private Entity entity;
	
	public TreeParser(Entity entity){
		this.entity = entity;
	}
	
	/**
	 * Loads a behavior tree from xml 
	 * @param filename
	 * @return A full behaviour tree
	 */
	public BehaviorTree parseTree(String filename){
		BehaviorTree tree = new BehaviorTree();
		
		try {
			 XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		      // Setup a new eventReader
		     InputStream in = new FileInputStream(filename);
		     XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			
		     Node root = null;
		     
		     while(eventReader.hasNext()){
		    	 XMLEvent event = eventReader.nextEvent();
		    	 
		    	 if(event.isStartElement()){
		    		 if(event.asStartElement().getName().getLocalPart() == "node"){
		    			 root = parseNode(eventReader);
		    		 }
		    	 }
		     }
		     tree.root = root;
		     tree.currentNode = root;
		     
		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		    } catch (XMLStreamException e) {
		      e.printStackTrace();
		    }
		
		return tree;
	}
	
	/**
	 * Helper function to recursively create the tree from xml
	 * @param eventReader
	 * @return The root node of a subtree
	 */
	private Node parseNode(XMLEventReader eventReader){
		Node node = null;
		
		boolean firstNode = true;
		NodeType type = null;
		SelectorType selectorType = null;
		int numTrials = 0;
		LeafType leafType = null;
		DecoratorType decType = null;
		
		ArrayList<Node> children = new ArrayList<Node>();
		
		try{
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				
				if(event.isStartElement()){
					StartElement startElement = event.asStartElement();
					
					if(startElement.getName().getLocalPart() == "node"){
							children.add(parseNode(eventReader));
					}
					
					if(startElement.getName().getLocalPart() == "nodeType"){
						event = eventReader.nextEvent();
						type = NodeType.valueOf(event.asCharacters().getData());
						continue;
					}
					
					if(startElement.getName().getLocalPart() == "selectorType"){
						event = eventReader.nextEvent();
						selectorType = SelectorType.valueOf(event.asCharacters().getData());
						continue;
					}
					
					if(startElement.getName().getLocalPart() == "decoratorType"){
						event = eventReader.nextEvent();
						decType = DecoratorType.valueOf(event.asCharacters().getData());
						continue;
					}
					
					if(startElement.getName().getLocalPart() == "leafType"){
						event = eventReader.nextEvent();
						leafType = LeafType.valueOf(event.asCharacters().getData());
						continue;
					}
					
					if(startElement.getName().getLocalPart() == "numTrials"){
						event = eventReader.nextEvent();
						numTrials = Integer.parseInt(event.asCharacters().getData());
						continue;
					}
				}
				
				if(event.isEndElement()){
					EndElement endElement = event.asEndElement();
					if(endElement.getName().getLocalPart() == "node"){
						switch(type){
						case SELECTOR:
							node = new Selector(entity, selectorType);
							break;
						case SEQUENCE:
							node = new Sequence(entity);
							break;
						case DECORATOR:
							node = new Decorator(entity, decType, numTrials);
							break;
						case LEAF:
							node = createLeaf(leafType);
							break;
						default:
							node = new Selector(entity, selectorType);
						}
						node.children = children;
						return node;
					}
				}
			}
	    } catch (XMLStreamException e) {
	      e.printStackTrace();
	    }
		return node;
	}
	
	private Leaf createLeaf(LeafType type){
		
		return null;
	}
}
