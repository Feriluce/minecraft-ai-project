package minecraftBot.behaviorTree;

import java.util.Random;

import minecraftBot.exceptions.NoReadyException;
import net.minecraft.entity.Entity;



/**
 *Implements the selector node of a behaviour tree.
 */
public class Selector extends Node {
	
	public SelectorType selectorType;

	public Selector(Entity entity, SelectorType type) {
		super(entity, NodeType.SELECTOR);
		this.selectorType = type;
	}
	
	public Selector(Selector sel){
		super(sel.entity, NodeType.SELECTOR);
		this.selectorType = sel.selectorType;
	}

	
	@Override
	public NodeStatus execute() throws NoReadyException{		
		if (this.status==NodeStatus.READY || this.status == NodeStatus.RUNNING) {
		switch(selectorType){
		case PRIORITY:
			for(Node child : this.children){
				NodeStatus childStatus = child.execute();
				if(childStatus == NodeStatus.FAIL){
					continue;
				}
				
				if(childStatus == NodeStatus.RUNNING){
					this.status = childStatus;
					return childStatus;
				}
				
				this.status = childStatus;
				return childStatus;
			}
			this.status = NodeStatus.FAIL;
			return this.status;
		case RANDOM:
			Random rng = new Random();
			NodeStatus childstat = this.children.get(rng.nextInt(children.size())).execute();
			this.status = childstat;
			return childstat;
		}
		
	}
		else throw new NoReadyException();
		
		return NodeStatus.FAIL;
	}
	
	
}
