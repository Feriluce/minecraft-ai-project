package minecraftBot.behaviorTree;

import minecraftBot.behaviorTree.leaves.ActionType;
import minecraftBot.behaviorTree.leaves.GeneralLeaf;
import minecraftBot.behaviorTree.leaves.Leaf;
import minecraftBot.behaviorTree.leaves.LeafType;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.entity.Entity;

public class BehaviorTree {
	public Node root;
	public Node currentNode;
	
	public BehaviorTree(BehaviorTree tree){
		this.root = copyTree(tree.root);
		this.currentNode = this.root;
	}
	
	public BehaviorTree(Node node){
		this.root = node;
		this.currentNode = node;
	}
	
	public BehaviorTree(){
		this.root = null;
		this.currentNode = null;
	}
	
	/**
	 * Helper function to copy a behavior tree.
	 * @param node
	 * @return
	 */
	private Node copyTree(Node node){
		Node root = Node.copyNode(node);
		
		
		for(Node child : node.children){
			root.children.add(copyTree(child));
		}
		
		return root;
	}
	
	
	/**
	 * Resets all nodes to ready, except for the ones currently running.
	 */
	public void resetTree(){
		if(root.status != NodeStatus.RUNNING){
			root.status = NodeStatus.READY;
		}
		
		for(Node child : root.children){
			resetTree(child);
		}
		return;
	}
	
	public void resetTree(Node curNode){
		if(curNode.status != NodeStatus.RUNNING){
			curNode.status = NodeStatus.READY;
		}
		
		for(Node child : curNode.children){
			resetTree(child);
		}
		
		return;
	}
	
	
	
	/**Method used to populate the main tree with all the nodes that we need. It calls the methods to also populate all the subtrees.
	 * @param entity
	 * @param basicStuffSubtree
	 * @param fightMonstersSubtree
	 * @param createStuffSubtree
	 * @param exploreSubtree
	 */
	public void populateMainTree(TheWallsBot entity){
		//System.out.println("Populate main tree");
		this.root= new Selector(entity, SelectorType.PRIORITY);
			Node fightMonsters= new Selector(entity, SelectorType.PRIORITY);
			this.populateFightMonstersSubtree(entity, fightMonsters);
			root.children.add(fightMonsters);
			
			Node dec1= new Decorator(entity, DecoratorType.TIMEDEC, 900000l);
			root.children.add(dec1); //this is the decorator to check that we are in the first 15 minutes of the game
				Node pr1= new Sequence(entity);
				dec1.children.add(pr1);
					Node basicSeq = new Sequence(entity);
					pr1.children.add(basicSeq);
						Node basicCond = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.BASIC_COND, null);
						basicSeq.children.add(basicCond);
						Node basicStuff= new Selector(entity, SelectorType.PRIORITY);
						this.populateBasicStuffSubtree(entity, basicStuff);
						basicSeq.children.add(basicStuff);
					Node rn1 = new Selector(entity, SelectorType.RANDOM);
					pr1.children.add(rn1);
						Node createStuff = new Selector(entity, SelectorType.PRIORITY);
						populateCreateStuffSubTree(entity, createStuff);
						rn1.children.add(createStuff);
						Node explore = new Selector(entity, SelectorType.PRIORITY);
						populateExploreSubTree(entity, explore);
						rn1.children.add(explore);
			Node dec2 = new Decorator(entity, DecoratorType.INVERTEDTIMEDEC,900000l);
				Node fightPlayers = new Leaf (entity, LeafType.ACTIONLEAF, ActionType.HUNTPLAYERS, null);
				dec2.children.add(fightPlayers);
		System.out.println("tree populated.");
	}
	
	

	
	
	/**method to populate the behavior subtree used to explore.
	 * @param entity
	 * @param tree
	 * @return 
	 */
	private void populateExploreSubTree(TheWallsBot entity, Node root) {
		//System.out.println("populate explore");
		//tree.root= new Selector (entity, SelectorType.RANDOM);
		Node seq1= new Sequence(entity);
		root.children.add(seq1);
		//Node seq2= new Sequence(entity);
		//root.children.add(seq2);
			Node dmnNear = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.DIAMONDNEAR_COND, null);
			seq1.children.add(dmnNear);
			Node collectDmn = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.COLLECTDIAMOND, null);
			seq1.children.add(collectDmn);
		Node seq3= new Sequence(entity);
		root.children.add(seq3);
		//Node seq4 = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.DIGDOWN, null);
		//root.children.add(seq4);
			Node ironNear = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.IRONNEAR_COND, null);
			seq3.children.add(ironNear);
			Node collectIron = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.COLLECTIRON, null);
			seq3.children.add(collectIron);
		Node seq5 = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.EXPLORE,null);
		root.children.add(seq5);
		
		
		
		//Node chestNear = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.CHESTNEAR, null);
		//seq2.children.add(chestNear);
		//Node collectChest = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.COLLECTCHEST, null);
		//seq2.children.add(collectChest);
		
		
	}

	/**method to populate the behavior subtree used to create stuff.
	 * @param entity
	 * @param tree
	 * @return
	 */
	private void populateCreateStuffSubTree(TheWallsBot entity, Node root) {
		//System.out.println("populate create stuff");
		//tree.root = new Selector(entity, SelectorType.RANDOM);
		Node seq1 = new Sequence(entity);
		root.children.add(seq1);
			Node isThereAll_diamondPickAxe = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.DIAMONDPICKAXE_COND, null);
			seq1.children.add(isThereAll_diamondPickAxe);
			Node dmnPickAxe = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.CREATEDMNPICKAXE, null);
			seq1.children.add(dmnPickAxe);
		Node seq2 = new Sequence(entity);
		root.children.add(seq2);
			Node isThereAll_IronPickAxe = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.IRONPICKAXE_COND, null);
			seq2.children.add(isThereAll_IronPickAxe);
			Node ironPickAxe = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.CREATEIRONPICKAXE, null);
			seq2.children.add(ironPickAxe);
	}

	
	/**method to populate the behavior subtree used to fight monsters.
	 * @param entity
	 * @param tree
	 * @return
	 */
	private void populateFightMonstersSubtree(TheWallsBot entity, Node root){
		//System.out.println("populate fight");
		//tree.root = new Selector(entity, SelectorType.RANDOM);
		Node enemiesNear = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.RUNAWAY_COND, null);
		root.children.add(enemiesNear);
		Node fight = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.FIGHT, null);
		root.children.add(fight);
		Node runAway = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.RUNAWAY, null);
		root.children.add(runAway);
		
	}
	
	
	/**method to populate the behavior subtree used for basic stuff.
	 * @param entity
	 * @param tree
	 * @return
	 */
	private void populateBasicStuffSubtree(TheWallsBot entity, Node root) {
		//System.out.println("populate basic");
		Node seq1 = new Sequence(entity);
		root.children.add(seq1);
			Node enoughWood = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.ENOUGHWOOD, null);
			seq1.children.add(enoughWood);
			Node collectWood = new Leaf(entity,LeafType.ACTIONLEAF, ActionType.COLLECTWOOD, null);
			seq1.children.add(collectWood);
		Node seq2 = new Sequence(entity);
		root.children.add(seq2);
			Node enoughStick = new Leaf(entity, LeafType.CONDITIONLEAF,ActionType.ENOUGHSTICK, null);
			seq2.children.add(enoughStick);
			Node createStick = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.CREATEWOODPICKAXE, null);
			seq2.children.add(createStick);
		Node seq3 = new Sequence(entity);
		root.children.add(seq3);
			Node isThereWood = new Leaf(entity, LeafType.CONDITIONLEAF,ActionType.WOODPICKAXE_COND, null);
			seq3.children.add(isThereWood);
			Node createWoodPickAxe = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.CREATEWOODPICKAXE, null);
			seq3.children.add(createWoodPickAxe);
		Node seq4 = new Sequence(entity);
		root.children.add(seq4);
			Node enoughStone = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.ENOUGHSTONE, null);
			seq4.children.add(enoughStone);
			Node collectStone = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.COLLECTSTONE, null);
			seq4.children.add(collectStone);
		Node seq5 = new Sequence(entity);
		root.children.add(seq5);
			Node isThereStone = new Leaf(entity, LeafType.CONDITIONLEAF, ActionType.STONEPICKAXE_COND, null);
			seq5.children.add(isThereStone);
			Node createStonePickAxe = new Leaf(entity, LeafType.ACTIONLEAF, ActionType.CREATESTONEPICKAXE, null);
			seq5.children.add(createStonePickAxe);
		
	}
}
