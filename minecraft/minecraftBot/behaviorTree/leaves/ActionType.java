package minecraftBot.behaviorTree.leaves;

/**
 * Types of actions that can be performed
 *
 */
public enum ActionType {
	DIGDOWN, COLLECTDIAMOND, COLLECTCHEST, COLLECTIRON, CREATEDMNPICKAXE, CREATEIRONPICKAXE, RUNAWAY, FIGHTWITHWOOD, 
	FIGHTWITHSTONE, FIGHTWITHIRON, FIGHTWITHDMN, COLLECTWOOD, CREATESTICK, CREATEWOODPICKAXE, COLLECTSTONE, CREATECRAFTINGTABLE,
	CREATESTONEPICKAXE, CREATEWOODPLANK, GOAROUND, DIAMONDNEAR_COND, WOODNEAR_COND, STONENEAR_COND, IRONNEAR_COND, CRAFTINGTABLE_COND, 
	DIAMONDPICKAXE_COND,  WOODPICKAXE_COND, STONEPICKAXE_COND, IRONPICKAXE_COND, FIGHTWOOD_COND, FIGHTSTONE_COND, FIGHTIRON_COND, 
	FIGHTDIAMOND_COND, PLANK_COND, STICK_COND,  TESTCONDITION, RUNAWAY_COND, BASIC_COND, EXPLORE, HUNTPLAYERS, FIGHT, ENOUGHWOOD, ENOUGHSTICK, ENOUGHSTONE,
	FIGHT_COND
	
}
