package minecraftBot.behaviorTree.leaves;

import java.util.ArrayList;
import java.util.List;

import minecraftBot.behaviorTree.BehaviorTree;
import minecraftBot.behaviorTree.Node;
import minecraftBot.behaviorTree.NodeStatus;
import minecraftBot.behaviorTree.NodeType;
import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import minecraftBot.entity.ai.EntityAICollectChest;
import minecraftBot.entity.ai.EntityAICollectDiamond;
import minecraftBot.entity.ai.EntityAICollectIron;
import minecraftBot.entity.ai.EntityAICollectStone;
import minecraftBot.entity.ai.EntityAICollectWood;
import minecraftBot.entity.ai.EntityAICraftingTable;
import minecraftBot.entity.ai.EntityAICreateDiamondPickAxe;
import minecraftBot.entity.ai.EntityAICreateIronPickAxe;
import minecraftBot.entity.ai.EntityAICreatePlanks;
import minecraftBot.entity.ai.EntityAICreateStick;
import minecraftBot.entity.ai.EntityAICreateStonePickAxe;
import minecraftBot.entity.ai.EntityAICreateWoodPickAxe;
import minecraftBot.entity.ai.EntityAIDigDown;
import minecraftBot.entity.ai.EntityAIExplore;
import minecraftBot.entity.ai.EntityAIFight;
import minecraftBot.entity.ai.EntityAIFightWithDiamondPickAxe;
import minecraftBot.entity.ai.EntityAIFightWithIronPickAxe;
import minecraftBot.entity.ai.EntityAIFightWithStonePickAxe;
import minecraftBot.entity.ai.EntityAIFightWithWoodPickAxe;
import minecraftBot.entity.ai.EntityAIGoAround;
import minecraftBot.entity.ai.EntityAIRunAway;
import minecraftBot.exceptions.NoReadyException;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

/**
 *Abstract class to represent a leaf node in the behavior tree.
 */
public class Leaf extends Node {

	private static final double DISTANCEfromENTITY = 0;
	public ActionType actionType;
	public LeafType leafType;
	//public BehaviorTree subTree;
	private TheWallsBot bot;
	
	public Leaf(TheWallsBot entity, LeafType leafType, ActionType actionType, BehaviorTree behaviorTree) {
		super(entity, NodeType.LEAF);
		this.leafType = leafType;
		this.actionType = actionType;
		//this.subTree = behaviorTree;
		bot = (TheWallsBot) entity;
		
		
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Static method to create a leaf from its type. May not work with all future leaves?
	 * All extra parameters could be chosen at random?
	 * @param entity
	 * @param type
	 * @return
	 */
	public static Leaf getLeaf(Entity entity, ActionType type){
		Leaf retVal = null;
		
		switch(type){
		default:
			
		}		
		return retVal;
	}
	

	/**
	 * @return Returns the result of executing a node.
	 * @throws NoReadyException 
	 */
	public NodeStatus execute() throws NoReadyException
	{
		//System.out.println("executing leaf");
		NodeStatus status=null;
		//System.out.println("spawning player: "+ bot.spawningPlayer);
			switch(actionType) {
			case COLLECTWOOD:
				
				System.out.println("collecting wood");
				EntityAICollectWood wood = new EntityAICollectWood((EntityLiving) super.entity,5);
				if (wood.shouldExecute()){
					boolean taskMatch1 = bot.currentTask.task instanceof EntityAICollectWood;
					return assignNodeStatus(wood, taskMatch1);
					}
				else status= NodeStatus.FAIL;
				break;
				/*boolean taskMatch = bot.currentTask.task instanceof EntityAICollectWood;
				
				if(bot.currentTask.status == TaskStatus.NONE){
					EntityAICollectWood wood = new EntityAICollectWood((EntityLiving) super.entity,5);
					bot.currentTask.task = wood;
					bot.currentTask.status = TaskStatus.RUNNING;
					bot.tasks.addTask(1, wood);
				} else if(taskMatch && bot.currentTask.status == TaskStatus.RUNNING){
					return NodeStatus.RUNNING;
				} else if(taskMatch && bot.currentTask.status != TaskStatus.RUNNING){
					bot.tasks.removeTask(bot.currentTask.task);
					bot.currentTask.task = null;
					bot.currentTask.status = TaskStatus.NONE;
					if(bot.currentTask.status == TaskStatus.SUCCEEDED){
						return NodeStatus.SUCCESS;
					} else {
						return NodeStatus.FAIL;
					}
				} else if(!taskMatch){
					bot.tasks.removeTask(bot.currentTask.task);
					EntityAICollectWood wood = new EntityAICollectWood((EntityLiving) super.entity,5);
					bot.currentTask.task = wood;
					bot.currentTask.status = TaskStatus.RUNNING;
					bot.tasks.addTask(1, wood);
				}*/
			case COLLECTDIAMOND:
				System.out.println("collecting diamond");
				EntityAICollectDiamond diamond = new EntityAICollectDiamond((EntityLiving) super.entity, 5);
				if (diamond.shouldExecute()){
					boolean taskMatch2 = bot.currentTask.task instanceof EntityAICollectDiamond;
					return assignNodeStatus(diamond, taskMatch2);
				}
				else status= NodeStatus.FAIL;
				break;
			
				/*
			case COLLECTCHEST:
				EntityAICollectChest chest = new EntityAICollectChest((EntityLiving) super.entity);
				if (chest.shouldExecute()){
					boolean taskMatch3 = bot.currentTask.task instanceof EntityAICollectChest;
					return assignNodeStatus(chest, taskMatch3);
				}
				else status= NodeStatus.FAIL;
				break;
				*/
			case COLLECTIRON:
				System.out.println("collecting iron");
				EntityAICollectIron iron = new EntityAICollectIron((EntityLiving) super.entity, 5);
				if (iron.shouldExecute()){
					boolean taskMatch4 = bot.currentTask.task instanceof EntityAICollectIron;
					return assignNodeStatus(iron, taskMatch4);
				}
				else status= NodeStatus.FAIL;
				break;
				
			case CREATEDMNPICKAXE:
				System.out.println("creating diamond pickaxe");
				EntityAICreateDiamondPickAxe dmnPickAxe = new EntityAICreateDiamondPickAxe((EntityLiving) super.entity,1);
				if (dmnPickAxe.shouldExecute()){
					boolean taskMatch5 = bot.currentTask.task instanceof EntityAICreateDiamondPickAxe;
					return assignNodeStatus(dmnPickAxe, taskMatch5);
				}
				else status= NodeStatus.FAIL;
				break;
				
			case CREATEIRONPICKAXE:
				System.out.println("creating iron pickaxe");
				EntityAICreateIronPickAxe ironPickAxe = new EntityAICreateIronPickAxe((EntityLiving) super.entity,1);
				if (ironPickAxe.shouldExecute()){
					boolean taskMatch6 = bot.currentTask.task instanceof EntityAICreateIronPickAxe;
					return assignNodeStatus(ironPickAxe, taskMatch6);
				}
				else status= NodeStatus.FAIL;
				break;
				
			case RUNAWAY:
				System.out.println("runaway");
				EntityAIRunAway runAway= new EntityAIRunAway((EntityLiving) super.entity);
				if (runAway.shouldExecute()){
					boolean taskMatch12 = bot.currentTask.task instanceof EntityAIRunAway;
					return assignNodeStatus(runAway, taskMatch12);
					}
				else status= NodeStatus.FAIL;
				break;
				
			case FIGHT:
				System.out.println("fighting");
				EntityAIFight fight= new EntityAIFight((EntityLiving) super.entity);
				if (fight.shouldExecute()){
					boolean taskMatch12 = bot.currentTask.task instanceof EntityAIFight;
					return assignNodeStatus(fight, taskMatch12);
					}
				else status= NodeStatus.FAIL;
				break;
				
			/*case FIGHTWITHSTONE:
				System.out.println("fighting with stone pickaxe");

				EntityAIFightWithStonePickAxe fightWithStone= new EntityAIFightWithStonePickAxe((EntityLiving) super.entity);
				if (fightWithStone.shouldExecute()){
					boolean taskMatch12 = bot.currentTask.task instanceof EntityAIFightWithStonePickAxe;
					return assignNodeStatus(fightWithStone, taskMatch12);
					}
				else status= NodeStatus.FAIL;
				break;
				
			case FIGHTWITHIRON:
				System.out.println("fighting with iron pickaxe");

				EntityAIFightWithIronPickAxe fightWithIron= new EntityAIFightWithIronPickAxe((EntityLiving) super.entity);
				if (fightWithIron.shouldExecute()){
					boolean taskMatch12 = bot.currentTask.task instanceof EntityAIFightWithIronPickAxe;
					return assignNodeStatus(fightWithIron, taskMatch12);
					}
				else status= NodeStatus.FAIL;
				break;
				
			case FIGHTWITHDMN:
				System.out.println("fighting with diamond pickaxe");

				EntityAIFightWithDiamondPickAxe fightWithDiamond= new EntityAIFightWithDiamondPickAxe((EntityLiving) super.entity);
				if (fightWithDiamond.shouldExecute()){
					boolean taskMatch12 = bot.currentTask.task instanceof EntityAIFightWithDiamondPickAxe;
					return assignNodeStatus(fightWithDiamond, taskMatch12);
					}
				else status= NodeStatus.FAIL;
				break;*/
				
			case CREATESTICK:
				System.out.println("creating stick");

				EntityAICreateStick stick = new EntityAICreateStick((EntityLiving) super.entity,1);
				if (stick.shouldExecute()){
					boolean taskMatch12 = bot.currentTask.task instanceof EntityAICreateStick;
					return assignNodeStatus(stick, taskMatch12);
					}
				else status= NodeStatus.FAIL;
				break;
			case CREATEWOODPICKAXE:
				System.out.println("creating wood pickaxe");

				EntityAICreateWoodPickAxe woodPickAxe = new EntityAICreateWoodPickAxe((EntityLiving) super.entity,1);
				if (woodPickAxe.shouldExecute()){
					boolean taskMatch13 = bot.currentTask.task instanceof EntityAICreateWoodPickAxe;
					return assignNodeStatus(woodPickAxe, taskMatch13);
					}
				else status= NodeStatus.FAIL;
				break;
			case COLLECTSTONE:
				System.out.println("collecting stone");

				EntityAICollectStone stone = new EntityAICollectStone((EntityLiving) super.entity,5);
				if (stone.shouldExecute()){
					boolean taskMatch14 = bot.currentTask.task instanceof EntityAICollectStone;
					return assignNodeStatus(stone, taskMatch14);
				}
				else status= NodeStatus.FAIL;
				break;
			case CREATECRAFTINGTABLE:
				System.out.println("creating crafting table");

				EntityAICraftingTable craftingTable = new EntityAICraftingTable((EntityLiving) super.entity,1);
				if (craftingTable.shouldExecute()){
					boolean taskMatch15 = bot.currentTask.task instanceof EntityAICraftingTable;
					return assignNodeStatus(craftingTable, taskMatch15);
				}
				else status= NodeStatus.FAIL;
				break;
			case CREATESTONEPICKAXE:
				System.out.println("creating stone  pickaxe");

				EntityAICreateStonePickAxe stonePickAxe = new EntityAICreateStonePickAxe((EntityLiving) super.entity,1);
				if (stonePickAxe.shouldExecute()){
					boolean taskMatch16 = bot.currentTask.task instanceof EntityAICreateStonePickAxe;
					return assignNodeStatus(stonePickAxe, taskMatch16);
				}
				else status= NodeStatus.FAIL;
				break;
			case CREATEWOODPLANK:
				System.out.println("creating wood planks");
				EntityAICreatePlanks plank = new EntityAICreatePlanks((EntityLiving) super.entity,1);
				if (plank.shouldExecute()){
					boolean taskMatch17 = bot.currentTask.task instanceof EntityAICreatePlanks;
					return assignNodeStatus(plank, taskMatch17);
				}
				else status= NodeStatus.FAIL;
				break;
			/*case DIGDOWN:
				System.out.println("digging down");
				EntityAIDigDown digDown = new EntityAIDigDown((EntityLiving) super.entity, 10);
				if (digDown.shouldExecute()){
					boolean taskMatch31 = bot.currentTask.task instanceof EntityAIDigDown;
					return assignNodeStatus(digDown, taskMatch31);
					}
				else status= NodeStatus.FAIL;
				break;
			case GOAROUND:
				System.out.println("going around");
				EntityAIGoAround goAround = new EntityAIGoAround((EntityLiving) super.entity, 15000, 0.47D); //// ??????? maxTime and speed
				if (goAround.shouldExecute()){
					boolean taskMatch32 = bot.currentTask.task instanceof EntityAIGoAround;
					return assignNodeStatus(goAround, taskMatch32);
				}
				else status= NodeStatus.FAIL;
				break;*/
			case EXPLORE:
				EntityAIExplore explore= new EntityAIExplore((EntityLiving) super.entity);
				if (explore.shouldExecute()){
					boolean taskMatch31 = bot.currentTask.task instanceof EntityAIExplore;
					return assignNodeStatus(explore, taskMatch31);
				}
				else status= NodeStatus.FAIL;
				break;
				
			case DIAMONDNEAR_COND:
				if(this.checkForResource(Block.oreDiamond.blockID)){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
				
			case IRONNEAR_COND:
				if(this.checkForResource(Block.oreIron.blockID)){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
			
			case WOODNEAR_COND:
				if(this.checkForResource(Block.wood.blockID)){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
				
			case STONENEAR_COND:
				if(this.checkForResource(Block.stone.blockID)){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
			
			case CRAFTINGTABLE_COND:
				if(bot.inventory.getItemCount(new ItemStack(Block.wood))>= 1 ){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
			
			case DIAMONDPICKAXE_COND:
				if(bot.inventory.getItemCount(new ItemStack(Block.oreDiamond))>=3 && bot.inventory.getItemCount(new ItemStack(Item.stick))>=2){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;			
			
			case IRONPICKAXE_COND:
				if(bot.inventory.getItemCount(new ItemStack(Block.oreIron))>=3 && bot.inventory.getItemCount(new ItemStack(Item.stick))>=2){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;	
			
			case WOODPICKAXE_COND:
				if(bot.inventory.getItemCount(new ItemStack(Block.planks))>=3 && bot.inventory.getItemCount(new ItemStack(Item.stick))>=2){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;	
			
			case STONEPICKAXE_COND:
				if(bot.inventory.getItemCount(new ItemStack(Block.stone))>=3 && bot.inventory.getItemCount(new ItemStack(Item.stick))>=2){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;	
			
			case PLANK_COND:
				if(bot.inventory.getItemCount(new ItemStack(Block.wood))>=1*(1/4 +1)){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
				
			case STICK_COND:
				if((bot.inventory.getItemCount(new ItemStack(Block.wood))>=1*(1/8 +1) || bot.inventory.getItemCount(new ItemStack(Block.planks))>=2*(1/4 +1))){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
				
			case FIGHT_COND:
				EntityAIFight e1= new EntityAIFight((EntityLiving) super.entity);
				if(bot.inventory.getItemCount(new ItemStack(Item.pickaxeWood))>=1 || bot.inventory.getItemCount(new ItemStack(Item.pickaxeStone))>0 || bot.inventory.getItemCount(new ItemStack(Item.pickaxeIron))>0 || bot.inventory.getItemCount(new ItemStack(Item.pickaxeDiamond))>0 && e1.findEnemies()){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
				
			/*case FIGHTDIAMOND_COND:
				EntityAIFightWithDiamondPickAxe e2= new EntityAIFightWithDiamondPickAxe((EntityLiving) super.entity);
				if(bot.inventory.getItemCount(new ItemStack(Item.pickaxeDiamond))>=1 && e2.findEnemies()){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
			
			case FIGHTIRON_COND:
				EntityAIFightWithIronPickAxe e3= new EntityAIFightWithIronPickAxe((EntityLiving) super.entity);
				if(bot.inventory.getItemCount(new ItemStack(Item.pickaxeIron))>=1 && e3.findEnemies()){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
				
			
			case FIGHTSTONE_COND:
				EntityAIFightWithStonePickAxe e= new EntityAIFightWithStonePickAxe((EntityLiving) super.entity);
				if(bot.inventory.getItemCount(new ItemStack(Item.pickaxeStone))>=1 && e.findEnemies()){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;*/
			
			case RUNAWAY_COND:
				EntityAIRunAway runAwayCond= new EntityAIRunAway((EntityLiving) super.entity);
				if(runAwayCond.shouldExecute()){
					return NodeStatus.SUCCESS;
				}else return NodeStatus.FAIL;
			case BASIC_COND:
				ItemStack pick = new ItemStack(Item.pickaxeStone);
				if(!(bot.inventory.getItemCount(pick) > 0)){
					return NodeStatus.SUCCESS;
				} else
					return NodeStatus.FAIL;
			case ENOUGHWOOD:
				ItemStack enoughWood = new ItemStack(Block.wood);
				if(!(bot.inventory.getItemCount(enoughWood) > 0)){
					return NodeStatus.SUCCESS;
				} else
					return NodeStatus.FAIL;
			case ENOUGHSTONE:
				ItemStack enoughStone = new ItemStack(Block.stone);
				if(!(bot.inventory.getItemCount(enoughStone) > 0)){
					return NodeStatus.SUCCESS;
				} else
					return NodeStatus.FAIL;
			case ENOUGHSTICK:
				ItemStack enoughStick = new ItemStack(Item.stick);
				if(!(bot.inventory.getItemCount(enoughStick) > 0)){
					return NodeStatus.SUCCESS;
				} else
					return NodeStatus.FAIL;
			
			default: 
				return NodeStatus.FAIL;
			}
			return status;
	}
	/*public NodeStatus execute() throws NoReadyException
	{
		NodeStatus status=null;
			switch(actionType) {
			case COLLECTWOOD:
				EntityAICollectWood wood = new EntityAICollectWood((EntityLiving) super.entity,5);
				boolean taskMatch1 = bot.currentTask.task instanceof EntityAICollectWood;
				return assignNodeStatus(wood, taskMatch1);
				
			case COLLECTDIAMOND:
				EntityAICollectDiamond diamond = new EntityAICollectDiamond((EntityLiving) super.entity);
				boolean taskMatch2 = bot.currentTask.task instanceof EntityAICollectDiamond;
				return assignNodeStatus(diamond, taskMatch2);
				
			case COLLECTCHEST:
				EntityAICollectChest chest = new EntityAICollectChest((EntityLiving) super.entity,5);
				boolean taskMatch3 = bot.currentTask.task instanceof EntityAICollectChest;
				return assignNodeStatus(chest, taskMatch3);
				
			case COLLECTIRON:
				EntityAICollectIron iron = new EntityAICollectIron((EntityLiving) super.entity);
				boolean taskMatch4 = bot.currentTask.task instanceof EntityAICollectIron;
				return assignNodeStatus(iron, taskMatch4);
				
			case CREATEDMNPICKAXE:
				EntityAICreateDiamondPickAxe dmnPickAxe = new EntityAICreateDiamondPickAxe((EntityLiving) super.entity,5);
				boolean taskMatch5 = bot.currentTask.task instanceof EntityAICreateDiamondPickAxe;
				return assignNodeStatus(dmnPickAxe, taskMatch5);
				
			case CREATEIRONPICKAXE:
				EntityAICreateIronPickAxe ironPickAxe = new EntityAICreateIronPickAxe((EntityLiving) super.entity,5);
				boolean taskMatch6 = bot.currentTask.task instanceof EntityAICreateIronPickAxe;
				return assignNodeStatus(ironPickAxe, taskMatch6);
				
			case RUNAWAY:
				
			case FIGHTWITHWOOD:
				
			case FIGHTWITHSTONE:
				
			case FIGHTWITHIRON:
				
			case FIGHTWITHDMN:
				
			case CREATESTICK:
				EntityAICreateStick stick = new EntityAICreateStick((EntityLiving) super.entity,5);
				boolean taskMatch12 = bot.currentTask.task instanceof EntityAICreateStick;
				return assignNodeStatus(stick, taskMatch12);
				
			case CREATEWOODPICKAXE:
				EntityAICreateWoodPickAxe woodPickAxe = new EntityAICreateWoodPickAxe((EntityLiving) super.entity,5);
				boolean taskMatch13 = bot.currentTask.task instanceof EntityAICreateWoodPickAxe;
				return assignNodeStatus(woodPickAxe, taskMatch13);

			case COLLECTSTONE:
				EntityAICollectStone stone = new EntityAICollectStone((EntityLiving) super.entity,5);
				boolean taskMatch14 = bot.currentTask.task instanceof EntityAICollectStone;
				return assignNodeStatus(stone, taskMatch14);
				
			case CREATECRAFTINGTABLE:
				EntityAICraftingTable craftingTable = new EntityAICraftingTable((EntityLiving) super.entity,5);
				boolean taskMatch15 = bot.currentTask.task instanceof EntityAICraftingTable;
				return assignNodeStatus(craftingTable, taskMatch15);
				
			case CREATESTONEPICKAXE:
				EntityAICreateStonePickAxe stonePickAxe = new EntityAICreateStonePickAxe((EntityLiving) super.entity,5);
				boolean taskMatch16 = bot.currentTask.task instanceof EntityAICreateStonePickAxe;
				return assignNodeStatus(stonePickAxe, taskMatch16);
				
			case CREATEWOODPLANK:
				EntityAICreatePlanks plank = new EntityAICreatePlanks((EntityLiving) super.entity,5);
				boolean taskMatch17 = bot.currentTask.task instanceof EntityAICreatePlanks;
				return assignNodeStatus(plank, taskMatch17);
				
			case DIAMONDNEAR:
				
			case CHESTNEAR:
				
			case IRONNEAR:
				
			case STICKDIAMOND:
				
			case STICKIRON:
			
			case ENEMIESNEAR:
				
			case WOODPICKAXE:
				
			case STONEPICKAXE:
				
			case IRONPICKAXE:
				
			case DMNPICKAXE:
				
			case WOODPLANK:
				
			case WOOD:
				
				
			case STICKSTONE:
				
			case STICKWOODPLANK:
				
				
			case DIGDOWN:
				EntityAIDigDown digDown = new EntityAIDigDown((EntityLiving) super.entity);
				boolean taskMatch31 = bot.currentTask.task instanceof EntityAIDigDown;
				return assignNodeStatus(digDown, taskMatch31);
				
			case GOAROUND:
				EntityAIGoAround goAround = new EntityAIGoAround((EntityLiving) super.entity, 5, 8); //// ??????? maxTime and speed
				boolean taskMatch32 = bot.currentTask.task instanceof EntityAIGoAround;
				return assignNodeStatus(goAround, taskMatch32);
			case TESTCONDITION:
				return NodeStatus.SUCCESS;
				
			default: return NodeStatus.READY;
			
			}
	}*/
	
	public NodeStatus assignNodeStatus(EntityAIBase entity, boolean task)
	{		
		if(bot.currentTask.status == TaskStatus.NONE){
			bot.currentTask.task = entity;
			bot.currentTask.status = TaskStatus.RUNNING;
			bot.tasks.addTask(1, entity);
		} else if(task && bot.currentTask.status == TaskStatus.RUNNING){
			return NodeStatus.RUNNING;
		} else if(task && bot.currentTask.status != TaskStatus.RUNNING){
			bot.tasks.removeTask(bot.currentTask.task);
			bot.currentTask.task = null;
			bot.currentTask.status = TaskStatus.NONE;
			if(bot.currentTask.status == TaskStatus.SUCCEEDED){
				return NodeStatus.SUCCESS;
			} else {
				return NodeStatus.FAIL;
			}
		} else if(!task){
			bot.tasks.removeTask(bot.currentTask.task);
			bot.currentTask.task = entity;
			bot.currentTask.status = TaskStatus.RUNNING;
			bot.tasks.addTask(1, entity);
		}
		return NodeStatus.READY;
	}
	
	private boolean checkForResource(int blockID){
		int woodX = 0;
		int woodY = 0;
		int woodZ = 0;
		World world = bot.worldObj;
		
		int posx = MathHelper.floor_double(bot.posX - 10);
		int posy = MathHelper.floor_double((bot.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(bot.posZ - 10);
		
		boolean foundResource = false;
		
		for(int y = 0; y < 5; y++){
			if(foundResource)
				break;
			for(int x = 0; x < 20; x++){
				if(foundResource)
					break;
				for(int z = 0; z < 20; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == blockID){
						woodX = posx+x;
						woodY = posy+y;
						woodZ = posz+z;
						foundResource = true;
						break;
					}
				}
			}
		}
		
		boolean seenResource = false;
		
		if(foundResource){
			Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(bot.posX),
					MathHelper.floor_double(bot.posY+bot.getEyeHeight()),
					MathHelper.floor_double(bot.posZ));
			Vec3 end = world.getWorldVec3Pool().getVecFromPool(woodX, woodY, woodZ);
			
			MovingObjectPosition hit = world.clip(start, end);
			
			if(hit == null || (hit.blockX == end.xCoord && hit.blockY == end.yCoord && hit.blockZ == end.zCoord)){ //TODO: Might be blocked  by target block. Maybe check for hit position vs target position
				seenResource = true;
			}
		}
		return seenResource;
		
	}
	
	
}
