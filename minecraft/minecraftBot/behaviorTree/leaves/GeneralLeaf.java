package minecraftBot.behaviorTree.leaves;

import minecraftBot.behaviorTree.BehaviorTree;
import minecraftBot.behaviorTree.NodeStatus;
import minecraftBot.entity.TheWallsBot;
import minecraftBot.exceptions.NoReadyException;
import net.minecraft.entity.Entity;



/**
 * This kind of leaf is used in the main tree to point to the subtrees.
 *
 */
public class GeneralLeaf extends Leaf {

	public GeneralLeaf(TheWallsBot entity, ActionType actionType, BehaviorTree behaviorTree) {
		
		super(entity, LeafType.GENERAL, actionType, behaviorTree);
	}

	
	
	@Override
	public NodeStatus execute() throws NoReadyException {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
