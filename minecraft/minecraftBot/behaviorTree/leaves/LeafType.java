package minecraftBot.behaviorTree.leaves;

public enum LeafType {
	ACTIONLEAF, CONDITIONLEAF, GENERAL
}
