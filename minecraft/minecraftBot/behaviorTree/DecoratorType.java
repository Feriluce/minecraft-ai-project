package minecraftBot.behaviorTree;

public enum DecoratorType {
	TIMEDEC, NUMITERATIONSDEC, INVERTEDTIMEDEC
}
