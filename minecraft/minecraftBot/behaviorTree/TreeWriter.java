package minecraftBot.behaviorTree;

import java.io.FileOutputStream;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import minecraftBot.behaviorTree.leaves.Leaf;

/**
 * Write a tree to XML for storage.
 * @author Feriluce
 *
 */
public class TreeWriter {
	
	String filename;
	
	public TreeWriter(String filename){
		this.filename = filename;
	}
	
	public void saveTree(BehaviorTree tree) throws Exception{
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		
		XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(new FileOutputStream(filename));
		
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
	    XMLEvent end = eventFactory.createDTD("\n");
	    
	    eventWriter.add(eventFactory.createStartDocument());
	    eventWriter.add(end);
	    eventWriter.add(eventFactory.createStartElement("", "", "root"));
	    eventWriter.add(end);
	        
	    createNode(eventWriter, tree.root);
	    
	    eventWriter.add(eventFactory.createEndElement("", "", "root"));
	    eventWriter.add(end);
	    eventWriter.add(eventFactory.createEndDocument());
	    eventWriter.close();
	}
	
	private void createNode(XMLEventWriter eventWriter, Node node) throws XMLStreamException {
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		XMLEvent end = eventFactory.createDTD("\n");
	    XMLEvent tab = eventFactory.createDTD("\t");
	    
	    StartElement nodeStartElement = eventFactory.createStartElement("", "", "node");
	    eventWriter.add(tab);
	    eventWriter.add(nodeStartElement);
	    eventWriter.add(end);
	    
	    switch(node.type){
	    case SELECTOR:
	    	Selector selector = (Selector) node;
	    	createField(eventWriter, "nodeType", node.type.name());
	    	createField(eventWriter, "selectorType", selector.selectorType.name());
	    	break;
	    case SEQUENCE:
	    	createField(eventWriter, "nodeType", node.type.name());
	    	break;
	    case DECORATOR:
	    	Decorator dec = (Decorator) node;
	    	createField(eventWriter, "nodeType", node.type.name());
	    	createField(eventWriter, "numTrials", Integer.toString(dec.numTrials));
	    	break;
	    case LEAF:
	    	Leaf leaf = (Leaf) node;
	    	createField(eventWriter, "nodeType", node.type.name());
	    	createField(eventWriter, "actionType", leaf.actionType.name());
	    	createField(eventWriter, "leafType", leaf.leafType.name());
	    	break;
	    }
	    
	    for(Node child : node.children){
	    	createNode(eventWriter, child);
	    }
	    
	    EndElement eElement = eventFactory.createEndElement("", "", "node");
	    eventWriter.add(eElement);
	    eventWriter.add(end);
	}
	
	private void createField(XMLEventWriter eventWriter, String name, String value) throws XMLStreamException {

	    XMLEventFactory eventFactory = XMLEventFactory.newInstance();
	    XMLEvent end = eventFactory.createDTD("\n");
	    XMLEvent tab = eventFactory.createDTD("\t");
	    
	    // create Start node
	    StartElement sElement = eventFactory.createStartElement("", "", name);
	    eventWriter.add(tab);
	    eventWriter.add(sElement);
	    
	    // create Content
	    Characters characters = eventFactory.createCharacters(value);
	    eventWriter.add(characters);
	    
	    // create End node
	    EndElement eElement = eventFactory.createEndElement("", "", name);
	    eventWriter.add(eElement);
	    eventWriter.add(end);

	  }
}
