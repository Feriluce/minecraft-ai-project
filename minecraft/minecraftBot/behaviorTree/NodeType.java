package minecraftBot.behaviorTree;

public enum NodeType {
	SELECTOR, SEQUENCE, DECORATOR, LEAF
}
