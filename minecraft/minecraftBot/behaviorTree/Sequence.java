package minecraftBot.behaviorTree;

import minecraftBot.exceptions.NoReadyException;
import net.minecraft.entity.Entity;

public class Sequence extends Node {
	 
	public Sequence (Entity entity)
	{
		super(entity, NodeType.SEQUENCE);
		this.status=NodeStatus.READY;
	}
	
	public Sequence(Sequence seq){
		super(seq.entity, seq.type);
		this.status=NodeStatus.READY;
	}
	
	@Override
	public NodeStatus execute() throws NoReadyException
	{
		//System.out.println("executing sequence");
		if (this.status==NodeStatus.READY) {
			for(Node child: this.children)
			{
				NodeStatus childStatus = child.execute();
				
				if(childStatus == NodeStatus.FAIL){
					this.status= childStatus;
					return childStatus;
				}
				
				if(childStatus == NodeStatus.RUNNING){
					this.status = childStatus;
					return childStatus;
				}
			}
			this.status= NodeStatus.SUCCESS;
			return this.status;
			}
		else if(this.status == NodeStatus.RUNNING){
			boolean foundRunning = false;
			for(Node child : this.children){
				if(!foundRunning && child.status != NodeStatus.RUNNING){
					continue;
				}
				foundRunning = true;
				
				NodeStatus childStatus = child.execute();
				if(childStatus == NodeStatus.FAIL){
					this.status= childStatus;
					return childStatus;
				}
				
				if(childStatus == NodeStatus.RUNNING){
					this.status = childStatus;
					return childStatus;
				}
			}
			this.status= NodeStatus.SUCCESS;
			return this.status;
		}
		else throw new NoReadyException();
	}
}

