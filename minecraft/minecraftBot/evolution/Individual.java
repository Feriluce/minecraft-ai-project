package minecraftBot.evolution;

import java.util.Comparator;

import minecraftBot.behaviorTree.BehaviorTree;
import minecraftBot.entity.TheWallsBot;

/**
 * Represents an individual, includes its behavior tree and its instance.
 */
public class Individual {
	public BehaviorTree tree;
	public TheWallsBot entity;
	public Offset offset;
	
	public Individual(BehaviorTree tree, TheWallsBot entity, Offset offset){
		this.tree = tree;
		this.entity = entity;
		this.offset = offset;
	}
	
	public static class IndividualComparator implements Comparator<Individual>{

		@Override
		public int compare(Individual lhs, Individual rhs) {
			int lhsFit = lhs.entity.fitnessTracker.evaluateFitness();
			int rhsFit = rhs.entity.fitnessTracker.evaluateFitness();
			
			if(lhsFit < rhsFit){
				return -1;
			} else if(lhsFit == rhsFit){
				return 0;
			} else {
				return 1;
			}
		}
	}
}
