package minecraftBot.evolution;

public class Offset {
	public int offsetX;
	public int offsetZ;
	
	public Offset(int x, int z){
		offsetX = x;
		offsetZ = z;
	}
}
