package minecraftBot.evolution.exploration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import minecraftBot.behaviorTree.BehaviorTree;
import minecraftBot.behaviorTree.Node;
import minecraftBot.behaviorTree.NodeType;
import minecraftBot.behaviorTree.Selector;
import minecraftBot.behaviorTree.SelectorType;
import minecraftBot.behaviorTree.Sequence;
import minecraftBot.behaviorTree.leaves.ActionType;
import minecraftBot.behaviorTree.leaves.Leaf;
import minecraftBot.entity.TheWallsBot;
import minecraftBot.evolution.Evolve;
import minecraftBot.evolution.Individual;
import minecraftBot.evolution.Offset;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Evolves the exploration behavior.
 */
public class EvolveExploration extends Evolve{
	//TODO: Move some of this code to superclass
	public static final int TREEDEPTH = 4;
	public static final int MAXNODES = 6;
	public static final float GRAFTCHANCE = 0.25f;
	public static final float MUTATIONCHANCE = 0.15f;
	public static final int GENERATIONSIZE = 3;
	public static final int MAXGENERATIONS = 100;
	public static final int MAPOFFSET = 70;
	
	public static final int CORNERX = -48;
	public static final int CORNERY = 2;
	public static final int CORNERZ = -48;
	public static final int DIMX = 63;
	public static final int DIMY = 87;
	public static final int DIMZ = 64;
	public static final int SPAWNX = -26;
	public static final int SPAWNY = 66;
	public static final int SPAWNZ = -29;
	
	public ArrayList<Offset> offsetList;
	
	//Expand this as needed!
	public ArrayList<ActionType> explorationActions; 
	
	public ArrayList<Individual> population;
	
	private Random rand;
	
	public EvolveExploration(){
		rand = new Random();
		population = new ArrayList<Individual>();
		explorationActions = new ArrayList<ActionType>(); 
		addActions();
		
		offsetList = new ArrayList<Offset>();
		offsetList.add(new Offset(MAPOFFSET, 0));
		offsetList.add(new Offset(MAPOFFSET, MAPOFFSET));
		//offsetList.add(new Offset(0, MAPOFFSET));
		//offsetList.add(new Offset(-MAPOFFSET, MAPOFFSET));
		//offsetList.add(new Offset(-MAPOFFSET, 0));
		//offsetList.add(new Offset(-MAPOFFSET, -MAPOFFSET));
		//offsetList.add(new Offset(0, -MAPOFFSET));
		offsetList.add(new Offset(MAPOFFSET, -MAPOFFSET));
	}
	
	/**
	 * Evolves a behavior tree used for exploration.
	 * @param targetFitness The target fitness score desired.
	 * @throws InterruptedException 
	 */
	public void evolve(World world, int targetFitness, EntityPlayer player) throws InterruptedException{
		Comparator<Individual> comp = new Individual.IndividualComparator();
		
		//TODO: Add ability to continue evolving from previous trees
		
		/*Orig map corners:
		 * X:-48, Y:2, Z:-48
		 * Dimensions: X:62 Y:86 Z:63
		 * 
		 * Spawn:X:-26, Y:66, Z:-29
		 */
		
		
		for(Offset os : offsetList){
			player.addChatMessage("Copying Map");
			copyMap(os.offsetX, os.offsetZ, world);
		}
		
	
		
		for(int i = 0; i < GENERATIONSIZE; i++){
			Individual ind = initTree(world, offsetList.get(i));
			population.add(ind);
			
			player.addChatMessage("Spawning Entity");
			ind.entity.setPosition(SPAWNX+ind.offset.offsetX, SPAWNY, SPAWNZ+ind.offset.offsetZ);
			world.spawnEntityInWorld(ind.entity);
		}
		
		boolean wait = false;
		
		while(population.get(population.size()-1).entity.fitnessTracker.lastFitness < targetFitness){
			if(System.currentTimeMillis() % 9999 == 0)
				wait = false;
			if(System.currentTimeMillis() % 10000 != 0 || wait){
				continue;
			}
			
			player.addChatMessage("Starting actual looop");
			Collections.sort(population, comp); //Sorting them updates their fitness as well, due to comparator
			
			//Remove worst member
			int worstIndex;
			Offset worstOffset = null;
			
			for(worstIndex = 0; worstIndex < population.size(); worstIndex++){
				if(population.get(worstIndex).entity.fitnessTracker.lastFitness == -1){
					continue;
				}
				player.addChatMessage("Removing crappy bot");
				worstOffset = population.get(worstIndex).offset;
				population.remove(worstIndex);
				break;
			}
			
			if(worstOffset == null){
				continue;
			}
			
			copyMap(worstOffset.offsetX, worstOffset.offsetZ, world);	
			
			BehaviorTree newTree = mateTrees(population.get(population.size()-1).tree, population.get(population.size()-2).tree);
			TheWallsBot newBot = new TheWallsBot(world);
			newBot.setPosition(SPAWNX+worstOffset.offsetX, SPAWNY, SPAWNZ+worstOffset.offsetZ);
			
			Individual newInd = new Individual(newTree, newBot, worstOffset);
			population.add(newInd);
			
			world.spawnEntityInWorld(newInd.entity);
			
			if(rand.nextFloat() < MUTATIONCHANCE){
				Individual luckyInd = population.get(rand.nextInt(population.size()));
				mutate(luckyInd, world);
			}
			wait = true;
		}
	}
	
	/**
	 * Initializes a random tree within the parameters.
	 * @return Random Tree
	 */
	private Individual initTree(World world, Offset offset){
		BehaviorTree tree = new BehaviorTree();
		int depth = rand.nextInt(TREEDEPTH)+1;
		int width = rand.nextInt(MAXNODES)+1;
		int curDepth = 1; //Starts at the depth after the root;
		Node root;
		TheWallsBot entity = new TheWallsBot(world);
		
		boolean coin = rand.nextBoolean();
		
		if(coin){
			SelectorType type = SelectorType.values()[rand.nextInt(SelectorType.values().length)];
			root = new Selector(entity, type);
		} else {
			root = new Sequence(entity);
		}
		
		expandSubTree(root, curDepth, depth, width, entity);
		
		tree.root = root;
		tree.currentNode = root;
		
		Individual retVal = new Individual(tree, entity, offset); 
		
		return retVal;
	}
	
	/**
	 * Recursive helper function to build a random tree.
	 * @param node
	 * @param curDepth
	 * @param maxDepth
	 * @param maxWidth
	 * @param entity
	 */
	private void expandSubTree(Node node, int curDepth, int maxDepth, int maxWidth, Entity entity){
		int curWidth = rand.nextInt(maxWidth)+1;
		
		if(curDepth == maxDepth){
			for(int i = 0; i < curWidth; i++){
				ActionType actionType = explorationActions.get(rand.nextInt(explorationActions.size()));
				Leaf leaf = Leaf.getLeaf(entity, actionType);
				node.children.add(leaf);
			}
			return;
		}
		
		for(int i = 0; i <curWidth; i++){
			NodeType type = NodeType.values()[rand.nextInt(NodeType.values().length)];
			
			while(type == NodeType.DECORATOR){
				type = NodeType.values()[rand.nextInt(NodeType.values().length)];
			}
			
			Node newNode;
			
			switch(type){
			case SELECTOR:
				SelectorType selType = SelectorType.values()[rand.nextInt(SelectorType.values().length)];
				newNode = new Selector(entity, selType);
				node.children.add(newNode);
				expandSubTree(newNode, curDepth+1, maxDepth, maxWidth, entity);
				break;
			case SEQUENCE:
				newNode = new Sequence(entity);
				node.children.add(newNode);
				expandSubTree(newNode, curDepth+1, maxDepth, maxWidth, entity);
				break;
			case LEAF:
				ActionType actionType = explorationActions.get(rand.nextInt(explorationActions.size()));
				Leaf leaf = Leaf.getLeaf(entity, actionType);
				node.children.add(leaf);
				break;
			default:
			}
		}
	}
	
	/**
	 * Mates two behavior trees together. One of their roots is chosen as the new root at random.
	 * A subtree of the 2nd tree is then grafted on to the first tree.
	 * @param tree1
	 * @param tree2
	 * @return
	 */
	private BehaviorTree mateTrees(BehaviorTree tree1, BehaviorTree tree2){
		BehaviorTree newTree1 = new BehaviorTree(tree1);
		BehaviorTree newTree2 = new BehaviorTree(tree2);
		BehaviorTree retVal = null;
		Node newRoot;
		Node otherRoot;
		
		boolean coin = rand.nextBoolean();
		
		if(coin){
			newRoot = newTree1.root;
			otherRoot = newTree2.root;
		} else {
			newRoot = newTree2.root;
			otherRoot = newTree2.root;
		}
		
		
		Node splitNode = otherRoot;
		
		while(!splitNode.children.isEmpty()){
			splitNode = splitNode.children.get(rand.nextInt(splitNode.children.size()));
			if(rand.nextFloat() < GRAFTCHANCE){
				break;
			}
		}
		
		Node graftNode = newRoot;
		
		while(!graftNode.children.isEmpty()){
			int index = rand.nextInt(graftNode.children.size());
			Node child = graftNode.children.get(index);
			if(child.children.isEmpty() || rand.nextFloat() < GRAFTCHANCE){
				graftNode.children.remove(index);
				graftNode.children.add(index, splitNode);
				break;
			}
			graftNode = child;
		}
		
		retVal = new BehaviorTree(newRoot);
		
		return retVal;
	}
	
	/*
	 * Copies a quadrant of the walls map to evaluate a bot on with the specified offset
	 */
	private void copyMap(int offsetX, int offsetZ, World world){
		for(int x = CORNERX; x <= CORNERX+DIMX;x++){
			for(int y = CORNERY; y <= CORNERY+DIMY;y++){
				for(int z = CORNERZ; z <= CORNERZ+DIMZ;z++){
					int id = world.getBlockId(x, y, z);
					TileEntity tile = world.getBlockTileEntity(x, y, z);
					int meta = world.getBlockMetadata(x, y, z);
					
					world.setBlock(x+offsetX, y, z+offsetZ, id, meta, 3);
					world.setBlockTileEntity(x+offsetX, y, z+offsetZ, tile);
				}
			}
		}
	}
	
	/*
	 * Mutates a tree.
	 */
	private void mutate(Individual ind, World world){
		BehaviorTree newTree = initTree(world, ind.offset).tree;
		
		Node splitNode = newTree.root;
		
		while(!splitNode.children.isEmpty()){
			splitNode = splitNode.children.get(rand.nextInt(splitNode.children.size()));
			if(rand.nextFloat() < GRAFTCHANCE){
				break;
			}
		}
		
		Node graftNode = ind.tree.root;
		
		while(!graftNode.children.isEmpty()){
			int index = rand.nextInt(graftNode.children.size());
			Node child = graftNode.children.get(index);
			if(child.children.isEmpty() || rand.nextFloat() < GRAFTCHANCE){
				graftNode.children.remove(index);
				graftNode.children.add(index, splitNode);
				break;
			}
			graftNode = child;
		}
	}
	
	private void addActions(){
		explorationActions.add(ActionType.DIGDOWN);
		explorationActions.add(ActionType.COLLECTDIAMOND);
		explorationActions.add(ActionType.COLLECTIRON);
		explorationActions.add(ActionType.COLLECTCHEST);
		explorationActions.add(ActionType.COLLECTWOOD);
		//explorationActions.add(ActionType.CHESTNEAR);
		explorationActions.add(ActionType.IRONNEAR_COND);
		explorationActions.add(ActionType.DIAMONDNEAR_COND);
		explorationActions.add(ActionType.GOAROUND);
	}


}









