package minecraftBot.evolution.exploration;

import minecraftBot.entity.TheWallsBot;
import minecraftBot.evolution.FitnessTracker;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class ExplorationFitnessTracker extends FitnessTracker {
	public static final long maxLifeSpan = 300000;
	public static final long cullingTime = 150000;
	public boolean ready; 
	
	private long startTime;
	
	
	public ExplorationFitnessTracker(TheWallsBot entity){
		super(entity);
		startTime = System.currentTimeMillis();
	}
	
	@Override
	public int evaluateFitness(){
		ItemStack iron = new ItemStack(Block.oreIron);
		ItemStack diamond = new ItemStack(Block.oreDiamond);
		
		int score = entity.inventory.getItemCount(iron) + entity.inventory.getItemCount(diamond)*5;
		long age = System.currentTimeMillis() - startTime;
		
		
		if(age > maxLifeSpan || (age > cullingTime && score < 5)){
			this.lastFitness = score;
			return score;
		}		
			
		return -1;
	}
}
