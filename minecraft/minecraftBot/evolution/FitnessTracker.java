package minecraftBot.evolution;

import java.util.Comparator;

import minecraftBot.entity.TheWallsBot;

/**
 * Object to track the fitness of a given entity
 * @author Feriluce
 *
 */
public abstract class FitnessTracker {
	public int lastFitness;
	
	protected TheWallsBot entity;
	
	public FitnessTracker(TheWallsBot entity){
		this.entity = entity;
		this.lastFitness = -1;
	}
	
	public abstract int evaluateFitness();
}
