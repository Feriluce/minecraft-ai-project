package minecraftBot.commands;

import minecraftBot.entity.TheWallsBot;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

public class CommandSpawnWallBot extends CommandBase {

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return "thewallsbot";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "/thewallsbot";
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] astring) {
		if(icommandsender instanceof EntityPlayer){
			EntityPlayer player = (EntityPlayer) icommandsender;
			TheWallsBot bot = new TheWallsBot(player.worldObj, player);
			bot.setPosition(player.posX+2, player.posY, player.posZ);
			player.worldObj.spawnEntityInWorld(bot);
			
		}

	}

}
