package minecraftBot.commands;

import minecraftBot.entity.TheWallsBot;
import minecraftBot.evolution.exploration.EvolveExploration;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;


/**
 * Adds a Command to start the simultaneous evolution.
 */
public class CommandEvolve extends CommandBase{

	@Override
	public String getCommandName() {
		return "evolve";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "/evolve <evolution_type> <target_fitness>";
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] args) {
		if(icommandsender instanceof EntityPlayer){
			EntityPlayer player = (EntityPlayer)icommandsender;
			World world = player.worldObj;
			EvolveExploration exploreEvolver = new EvolveExploration();
			int fitness = 0;
			
			if(args.length == 2){
				try{
					fitness = Integer.parseInt(args[1]);
				} catch (NumberFormatException e){
					player.addChatMessage("Fitness value is not an int");
					return;
				}
				
				if(args[0].equals("exploration")){
					player.addChatMessage("This actually works");
					
					try{
						exploreEvolver.evolve(world, fitness, player);
					} catch (InterruptedException e){
						player.addChatMessage(e.getMessage());
					}
					Entity entity = new TheWallsBot(world);
					entity.setPosition(
							player.posX+2,
							player.posY,
							player.posZ
							);
					world.spawnEntityInWorld(entity);
					
					
				}
			}
		}
	}

}
