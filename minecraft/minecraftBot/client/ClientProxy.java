package minecraftBot.client;

import minecraftBot.CommonProxy;
import minecraftBot.entity.TheWallsBot;
import minecraftBot.entity.TheWallsBot.RenderWallBot;
import aTesting.ModelCustomSteve;
import cpw.mods.fml.client.registry.RenderingRegistry;
 
public class ClientProxy extends CommonProxy {
       
        @Override
        public void registerRenderers() {
                // This is for rendering entities and so forth later on
        	RenderingRegistry.registerEntityRenderingHandler(TheWallsBot.class, new RenderWallBot(new ModelCustomSteve(), 0.5F));
        }
       
}