package minecraftBot.inventory;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Inventory class. A virtual inventory for the bot to keep track of its item.
 */
public class Inventory {
	private ArrayList<ItemStack> inventory;
	
	public Inventory(){
		this.inventory = new ArrayList<ItemStack>();
	}
	
	/**
	 * Adds an itemstack to the inventory
	 * @param block
	 */
	
	public void addItems(ItemStack items){
		boolean found = false;
		
		for(ItemStack invStack : inventory){
			if(invStack.itemID == items.itemID){
				found = true;
				invStack.stackSize += items.stackSize;
				break;
			}
		}
		
		if(!found){
			inventory.add(items);
		}
	}
	
	/**
	 * Removes an itemstack from the inventory. Returns false if it doesn't exist, or the total items would be below 0;
	 */
	public boolean removeItems(ItemStack items){
		
		for(ItemStack invStack : inventory){
			if(invStack.itemID == items.itemID){
				int stackDif = invStack.stackSize - items.stackSize;
				if(stackDif > 0){
					invStack.stackSize = stackDif;
					return true;
				} else if(stackDif == 0) {
					inventory.remove(invStack);
					return true;
				} else if(stackDif < 0){
					return false;
				}
			}
		}
		return false;
	}
	
	public int getItemCount(ItemStack item){
		for(ItemStack invStack : inventory){
			if(invStack.itemID == item.itemID){
				return invStack.stackSize;
			}
		}
		
		return 0;
	}
	
	public ArrayList<ItemStack> getItemStackList(){
		return inventory;		
	}
	
	public ItemStack getItemStack(int itemID){
		for(ItemStack items : inventory){
			if(items.itemID==itemID){
				return items;
			}
		}
		return null;
	}
	
}
