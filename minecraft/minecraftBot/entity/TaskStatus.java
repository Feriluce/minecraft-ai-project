package minecraftBot.entity;

public enum TaskStatus {
	RUNNING, SUCCEEDED, FAILED, NONE
}
