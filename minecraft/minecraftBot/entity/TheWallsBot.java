package minecraftBot.entity;

import minecraftBot.behaviorTree.BehaviorTree;
import minecraftBot.entity.ai.EntityAIRunTree;
import minecraftBot.evolution.FitnessTracker;
import minecraftBot.evolution.exploration.ExplorationFitnessTracker;
import minecraftBot.inventory.Inventory;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TheWallsBot extends EntityMob{
	public RunningTask currentTask;
	public Inventory inventory;
	public FitnessTracker fitnessTracker;
	public BehaviorTree mainTree;
	public EntityPlayer spawningPlayer;

	
	
	public TheWallsBot(World par1World) {
		super(par1World);
		
		this.currentTask = new RunningTask();
		this.inventory= new Inventory();
		this.fitnessTracker = new ExplorationFitnessTracker(this);
		this.getNavigator().setBreakDoors(true);
		this.getNavigator().setEnterDoors(true);
		this.tasks.addTask(0, new EntityAISwimming(this));
        //this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        //this.tasks.addTask(2, new EntityAICollectWood(this, 4));
		
		createTree();
        //this.item = null;
	}
	
	public TheWallsBot(World par1World, EntityPlayer player){
		super(par1World);
		this.currentTask = new RunningTask();
		this.inventory= new Inventory();
		this.fitnessTracker = new ExplorationFitnessTracker(this);
		this.getNavigator().setBreakDoors(true);
		this.getNavigator().setEnterDoors(true);
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(10, new EntityAIRunTree(this));
        //this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        //this.tasks.addTask(2, new EntityAICollectWood(this, 4));
		this.spawningPlayer = player;
		
		createTree();

        //this.item = null;
        
		
	}

	/**
	 * method used to create tree and subtrees.
	 */
	private void createTree(){
		System.out.println("Creating the tree");
		mainTree= new BehaviorTree();		
		mainTree.populateMainTree(this);
	}
	
	protected boolean isAIEnabled()
    {
        return true;
    }
	
	protected void entityInit()
    {
        super.entityInit();
    }
	
	public void onLivingUpdate(){
		//Should work for updating our behaviorTree every frame.
		/*
		try{
			mainTree.root.execute();
			
		} catch (NoReadyException e){
			System.out.println("meh");
		}
		//testTree.resetTree();
		
		if(mainTree!= null ){ //&& basicStuffSubtree != null && fightMonstersSubtree != null && createStuffSubtree != null){
			mainTree.resetTree();
			//basicStuffSubtree.resetTree();
			//fightMonstersSubtree.resetTree();
			//createStuffSubtree.resetTree();
		}*/
		super.onLivingUpdate();
	}
	
	@SideOnly(Side.CLIENT)
    public static class RenderWallBot extends RenderLiving{
    	
    	private static final ResourceLocation zombieTextures = new ResourceLocation("basic", "testSkin.png");

		public RenderWallBot(ModelBase par1ModelBase, float par2) {
			super(par1ModelBase, par2);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected ResourceLocation getEntityTexture(Entity entity) {
			// TODO Auto-generated method stub
			return zombieTextures;
		}
    	
    }
	
	public EntityPlayer getSpawningPlayer(){
		return this.spawningPlayer;
	}
	
	
}
