package minecraftBot.entity.ai;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;

public class EntityAIRunAwaySelector implements IEntitySelector{
	
	
	 final EntityAIRunAway entityRunAwayAI;

	 EntityAIRunAwaySelector(EntityAIRunAway entity)
	    {
	        this.entityRunAwayAI = entity;
	    }

	    /**
	     * Return whether the specified entity is applicable to this filter.
	     */
	    public boolean isEntityApplicable(Entity par1Entity)
	    {
	        return par1Entity.isEntityAlive() && EntityAIRunAway.getEntityToAvoid(this.entityRunAwayAI).getEntitySenses().canSee(par1Entity);
	    }
	

}
