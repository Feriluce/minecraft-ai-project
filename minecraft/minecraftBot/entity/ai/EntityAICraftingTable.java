package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityAICraftingTable extends EntityAIBase{

	private TheWallsBot entity;
	private World world;
	private int amount;
	private int numCreated=0;
	
	
	public EntityAICraftingTable(EntityLiving entity, int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.amount=amount;
	}
	
	@Override
	public boolean shouldExecute() {
		if(entity.inventory.getItemCount(new ItemStack(Block.wood))>= 1*amount ){
			return true;
		}
		this.entity.currentTask.status = TaskStatus.FAILED;
		//System.out.println("There isn't enough wood to create "+amount+" Crafting table.");
		return false;
	}
	
	public boolean continueExecuting(){
		if(numCreated<amount){
			createCraftingTable();
			return true;
		} else {
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}
	}
	
	private void createCraftingTable(){
		
		System.out.println("Starting crafting table creation");
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.workbench))+" CraftingTable. " );
		
		ItemStack itemsToRemove= new ItemStack(Block.wood, 1);
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(itemsToRemove)+"blocks of "+ Block.wood.toString());
		entity.inventory.removeItems(itemsToRemove);
		System.out.println("Items removed from the inventory");
		entity.inventory.addItems(new ItemStack(Block.workbench));
		System.out.println("Item added to the inventory. The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.workbench))+" blocks of CraftingTable" );
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(itemsToRemove)+"blocks of "+ Block.wood.toString());

		numCreated++;
	}
	
	public void startExecuting(){
		if(shouldExecute()){
			entity.currentTask.task = this;
			entity.currentTask.status = TaskStatus.RUNNING;
			createCraftingTable();
		}
		
	}
	

}
