package minecraftBot.entity.ai;

import java.util.ArrayList;
import java.util.List;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAICollectStone extends EntityAIBase{

		
	private double speed = 0.47;	
	long timeOut = 4500;//ms
	long startTime= 0;
	boolean lock;
	private TheWallsBot entity;
	private World world;
	private int numCollected=0;
	private int amountNeeded;
	private boolean foundStone = false;	
	private int stoneX=0;
	private int stoneY=0;
	private int stoneZ=0;
	
	
	public EntityAICollectStone(EntityLiving entity,int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.setMutexBits(1);
		this.amountNeeded = amount;
		lock = false;
	}


	@Override
	public boolean shouldExecute() {
		//if(!lock){
			//lock = true;
			return numCollected < amountNeeded;
		//}
		//this.entity.currentTask.status = TaskStatus.FAILED;
		//return false;
		
	}
	
	
	
	public boolean continueExecuting(){
		if(numCollected >= amountNeeded){
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}		
		
		if(System.currentTimeMillis() > startTime+timeOut){
			this.entity.currentTask.status = TaskStatus.FAILED;
			System.out.println("Didnt find stone");
			return false;
		}
		
		boolean foundStone = true;
		
		
		if(this.entity.getNavigator().noPath()){
			foundStone = findStone();
		}
		
		if(!foundStone || this.entity.getNavigator().noPath()){
			Vec3 vec = RandomPositionGenerator.findRandomTarget(entity, 20, 4);
			entity.getNavigator().tryMoveToXYZ(vec.xCoord, vec.yCoord, vec.zCoord, speed);
			return true;
		}
		
		collectStone();
		return true;
		
	}
	
	public void startExecuting(){
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		startTime = System.currentTimeMillis();
		findStone();
		collectStone();
		System.out.println("How often does this happen?");
		
	}
	
	
	private boolean findStone(){
		int posx = MathHelper.floor_double(entity.posX - 10);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 10);
		
		boolean foundStone = searchLoop(posx, posy, posz);;
		
		
		if(foundStone){
			Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(entity.posX),
					MathHelper.floor_double(entity.posY+entity.getEyeHeight()),
					MathHelper.floor_double(entity.posZ));
			Vec3 end = world.getWorldVec3Pool().getVecFromPool(stoneX, stoneY, stoneZ);
			MovingObjectPosition hit = world.clip(start, end);
			
			if(hit == null || (hit.blockX == end.xCoord && hit.blockY == end.yCoord && hit.blockZ == end.zCoord)){ //TODO: Might be blocked  by target block. Maybe check for hit position vs target position
				this.entity.getNavigator().tryMoveToXYZ(stoneX, stoneY, stoneZ, speed);
			}
			
		}
		return foundStone;
	}
	
	
	private boolean searchLoop(int posx, int posy, int posz){
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 20; x++){
				for(int z = 0; z < 20; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.stone.blockID){
						stoneX = posx+x;
						stoneY = posy+y;
						stoneZ = posz+z;
						foundStone = true;
						System.out.println("Stone has been found.");
						return true;
					}
				}
			}
		}
		return false;
	}
	
	
	private void collectStone(){		
		int posx = MathHelper.floor_double(entity.posX -1);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 1);
		
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 3; x++){
				for(int z = 0; z < 3; z++){
					if(world.getBlockId(posx+x, posy+y, posz+z) == Block.leaves.blockID)
						world.setBlock(posx+x, posy+y, posz+z, 0);
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.stone.blockID){
						ItemStack stack = new ItemStack(Block.stone);
						entity.inventory.addItems(stack);
						numCollected++;
						world.setBlock(posx+x, posy+y, posz+z, 0);
					}
				}
			}
		}				
	}
	
	
	

}


