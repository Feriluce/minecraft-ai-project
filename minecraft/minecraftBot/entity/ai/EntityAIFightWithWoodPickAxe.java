package minecraftBot.entity.ai;

import java.util.ArrayList;
import java.util.List;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAIFightWithWoodPickAxe extends EntityAIBase{
	private static final double DISTANCEfromENTITY = 3;
	private TheWallsBot entity;
	private World world;
	private Class targetEntityClass= EntityMob.class;
	private Entity closestLivingEntity=null;
	private EntityAINearestTargetSelector selector;
	private Object entityPathNavigate;
	private Object entityPathEntity;
	double speed = 0.47;
	boolean dead=false;
	private List list;
	
	
	public EntityAIFightWithWoodPickAxe(EntityLiving entity){
		this.entity=(TheWallsBot) entity;
		this.world = entity.worldObj;
		this.setMutexBits(1);
		selector=new EntityAINearestTargetSelector(this, (TheWallsBot) entity);
	}
	
	@Override
	public boolean shouldExecute() {
		boolean ret=false;
		if(entity.inventory.getItemCount(new ItemStack(Item.pickaxeWood))>0){
           ret=findEnemies();
		}else{
			System.out.println("You don't have this weapon.");
			ret= false;
		}
		entity.currentTask.status = TaskStatus.FAILED;
		return ret;
	}
	
	
	
	 public void startExecuting()
	    {
		 this.entity.setCurrentItemOrArmor(0, entity.inventory.getItemStack(Item.pickaxeWood.itemID));
		 if(closestLivingEntity!=null){
	    	entity.currentTask.task = this;
			entity.currentTask.status = TaskStatus.RUNNING;
			goANDfightClosestEntity();
		 }
	    }
	 
	 public boolean continueExecuting()
	    {
			if(this.closestLivingEntity.isDead){
			 if(findEnemies()){
				 goANDfightClosestEntity();
				 return true;
			 }else {
				 this.entity.currentTask.status = TaskStatus.SUCCEEDED;	
					return false;
			 }
		 }else if(!entity.isDead) {
			 goANDfightClosestEntity();
			 return true;
		 }else {
			 this.entity.currentTask.status = TaskStatus.FAILED;	
			 return false;
		 }
	    }
     
	 
	 public void goANDfightClosestEntity(){		 
		 Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(entity.posX),
					MathHelper.floor_double(entity.posY+entity.getEyeHeight()),
					MathHelper.floor_double(entity.posZ));
		Vec3 end = world.getWorldVec3Pool().getVecFromPool(closestLivingEntity.posX, closestLivingEntity.posY, closestLivingEntity.lastTickPosZ);
		MovingObjectPosition hit = world.clip(start, end);
		if(hit == null || (hit.blockX == end.xCoord && hit.blockY == end.yCoord && hit.blockZ == end.zCoord)){ 
			this.entity.getNavigator().tryMoveToXYZ(closestLivingEntity.posX, closestLivingEntity.posY, closestLivingEntity.posZ, speed);
		}				
		
		 this.entity.attackEntityAsMob(this.entity.getAttackTarget());	
	 }
	 
	 
	 public boolean findEnemies(){
		 list = this.entity.worldObj.selectEntitiesWithinAABB(this.targetEntityClass, this.entity.boundingBox.expand((double)this.DISTANCEfromENTITY, 3.0D, (double)this.DISTANCEfromENTITY), this.selector);
		 
		 EntityPlayer player= findClosestPlayer();
		 
		 
		 
		 if (list.isEmpty() )
         {        
			 if(player==null ){
				 return false;
			 }else this.closestLivingEntity = player; 
             
         }else{
        	 if(player==null){
        		 this.closestLivingEntity = (Entity)list.get(0);  
        	 }else {
        		 if(entity.getDistanceToEntity(player)>entity.getDistanceToEntity((Entity)list.get(0))){
            		 this.closestLivingEntity = (Entity)list.get(0);  
            	 }else {
            		 this.closestLivingEntity = player;  
            	 }
        	 }
         }
         	entity.setAttackTarget((EntityLivingBase) closestLivingEntity);
 			return true;
         }
            
	 private EntityPlayer findClosestPlayer(){		
			ArrayList<EntityPlayer> players = (ArrayList<EntityPlayer>) world.playerEntities;
			 EntityPlayer closestPlayer=null;
			 
			 
			 
			 float minDistance=Float.MAX_VALUE;
			 if(players.size()>0){
				 for(EntityPlayer p: players){			
					 if(this.entity.getSpawningPlayer() == null){
						 if(entity.getDistanceToEntity(p)<minDistance){
							 closestPlayer=p;
							 minDistance=entity.getDistanceToEntity(p);
						 }					 
						 continue;
					 }
					 if(entity.getDistanceToEntity(p)<minDistance && ( p.getTeam()!=this.entity.getSpawningPlayer().getTeam() )){
						 closestPlayer=p;
						 minDistance=entity.getDistanceToEntity(p);
					 }
				 }
			 }
			 return closestPlayer;
		}


}
