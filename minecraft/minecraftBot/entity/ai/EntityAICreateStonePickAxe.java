package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityAICreateStonePickAxe extends EntityAIBase{

	private TheWallsBot entity;
	private World world;
	private int amount;
	private int numCreated=0;
	

	public EntityAICreateStonePickAxe(EntityLiving entity, int amount){
		this.entity= (TheWallsBot) entity;
		this.world = entity.worldObj;
		this.amount=amount;		
		//mutexBit
	}
	
	@Override
	public boolean shouldExecute() {		
		if(entity.inventory.getItemCount(new ItemStack(Block.stone))>=3*amount && entity.inventory.getItemCount(new ItemStack(Item.stick))>=2*amount){
			return true;
		}	
		//System.out.println("There aren't the raw materials to create "+amount+" stone pick axe.");		
		entity.currentTask.status = TaskStatus.FAILED;
		return false;
	}

	
	public boolean continueExecuting(){		
		if(numCreated<amount){
			createPickAxe();
			return true;
		} else {
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}
	}
	
	public void startExecuting(){
		if(shouldExecute()){
			entity.currentTask.task = this;
			entity.currentTask.status = TaskStatus.RUNNING;
			createPickAxe();
		}		
	}
		
	public void createPickAxe(){
		System.out.println("Starting stone pick axe creation");
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.stone))+"blocks of "+ Block.stone.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Item.stick))+"blocks of "+ Item.stick.toString());		
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Item.pickaxeStone))+Item.pickaxeStone.toString());

		entity.inventory.removeItems( new ItemStack(Block.stone, 3 ));
		entity.inventory.removeItems( new ItemStack(Item.stick, 2 ));
		entity.inventory.addItems(new ItemStack(Item.pickaxeStone));
		numCreated++;	
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Item.pickaxeStone))+Item.pickaxeStone.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.stone))+"blocks of "+ Block.stone.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Item.stick))+"blocks of "+ Item.stick.toString());		
		
	}	
	
}
