package minecraftBot.entity.ai;

import java.util.ArrayList;
import java.util.List;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.util.Vec3;

public class EntityAIRunAway extends EntityAIBase{

    public final IEntitySelector selector = new EntityAIRunAwaySelector(this);
    public final double SPEED=1;
	
	private static float DISTANCEfromENTITY = 2;
	private TheWallsBot entity;
	/**
	 * this is the entity to avoid
	 */
	private EntityCreature theEntity;
	
	/** The class of the entity we should avoid */
    private Class targetEntityClass= EntityMob.class;
    
    /** The PathNavigate of our entity */
    private PathNavigate entityPathNavigate;
    
    /**entity to avoid*/
    private Entity closestLivingEntity;	    
	
    /** The PathEntity of our entity */
	private PathEntity entityPathEntity;

	
	
    public EntityAIRunAway(EntityLiving entity2)
	 {
	    this.entity = (TheWallsBot) entity2;      
	    this.entityPathNavigate = entity2.getNavigator();
	    this.setMutexBits(1);
	 }

   
    //TODO: do we have to run away also from players??
    public boolean shouldExecute()
    {        
    	boolean ret=false;
            List list = this.entity.worldObj.selectEntitiesWithinAABB(this.targetEntityClass, this.entity.boundingBox.expand((double)this.DISTANCEfromENTITY, 3.0D, (double)this.DISTANCEfromENTITY), this.selector);
            EntityPlayer player= findClosestPlayer();
            
            if (list.isEmpty() )
            {        
   			 if(player==null ){
   				 ret=false;
   			 }else this.closestLivingEntity = player; 
                
            }else{
           	 if(player==null){
           		 this.closestLivingEntity = (Entity)list.get(0);  
           	 }else {
           		 if(entity.getDistanceToEntity(player)>entity.getDistanceToEntity((Entity)list.get(0))){
               		 this.closestLivingEntity = (Entity)list.get(0);  
               	 }else {
               		 this.closestLivingEntity = player;  
               	 }
           	 }
            }

        Vec3 vec3 = RandomPositionGenerator.findRandomTargetBlockAwayFrom(this.entity, 16, 7, this.entity.worldObj.getWorldVec3Pool().getVecFromPool(this.closestLivingEntity.posX, this.closestLivingEntity.posY, this.closestLivingEntity.posZ));

        if (vec3 == null)
        {
            ret= false;
        }
        else if (this.closestLivingEntity.getDistanceSq(vec3.xCoord, vec3.yCoord, vec3.zCoord) < this.closestLivingEntity.getDistanceSqToEntity(this.entity))
        {
            ret= false;
        }
        else
        {
            this.entityPathEntity = this.entityPathNavigate.getPathToXYZ(vec3.xCoord, vec3.yCoord, vec3.zCoord);
            ret= this.entityPathEntity == null ? false : this.entityPathEntity.isDestinationSame(vec3);
        }
        if (ret==false){
        	entity.currentTask.status = TaskStatus.FAILED;
            return false;
        }else return true;
    }

	
	    
	    public boolean continueExecuting()
	    {
	    	if(!this.shouldExecute()){
				this.entity.currentTask.status = TaskStatus.SUCCEEDED;
				return false;
	    	}
	        return true; 
	    }
	    
	    
	    public void startExecuting()
	    {
	    	entity.currentTask.task = this;
			entity.currentTask.status = TaskStatus.RUNNING;
	        this.entityPathNavigate.setPath(this.entityPathEntity, this.SPEED);
	    }
	    
	    static EntityCreature getEntityToAvoid(EntityAIRunAway entityAI)
	    {
	        return entityAI.entity;
	    }
	    
	    private EntityPlayer findClosestPlayer(){		
			ArrayList<EntityPlayer> players = (ArrayList<EntityPlayer>) entity.worldObj.playerEntities;
			 EntityPlayer closestPlayer=null;
			 
			 
			 
			 float minDistance=Float.MAX_VALUE;
			 if(players.size()>0){
				 for(EntityPlayer p: players){			
					 if(this.entity.getSpawningPlayer() == null){
						 if(entity.getDistanceToEntity(p)<minDistance){
							 closestPlayer=p;
							 minDistance=entity.getDistanceToEntity(p);
						 }					 
						 continue;
					 }
					 if(entity.getDistanceToEntity(p)<minDistance && ( p.getTeam()!=this.entity.getSpawningPlayer().getTeam() )){
						 closestPlayer=p;
						 minDistance=entity.getDistanceToEntity(p);
					 }
				 }
			 }
			 return closestPlayer;
		}
	
	

}
