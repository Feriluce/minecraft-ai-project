package minecraftBot.entity.ai;



import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

/**
 * Behavior to collect Wood from trees and explore the environment if it cannot find any.
 * @author Feriluce
 *
 */
public class EntityAICollectWood extends EntityAIBase{
	//TODO: Fix LoS issues. Possibly add raytraces on either side to improve accuracy?
	// Use return type of clip to debug.
	TheWallsBot entity;
	World world;
	double speed = 0.47;
	double woodX = 0;
	double woodY = 0;
	double woodZ = 0;
	int numCollected = 0;
	int amountNeeded;
	long timeOut = 4500;//ms
	long startTime= 0;
	boolean lock;
	
	public EntityAICollectWood(EntityLiving entity,int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.setMutexBits(1);
		this.amountNeeded = amount;
		lock = false;
	}
	
	@Override
	public boolean shouldExecute() {
		//if(!lock){
		//	lock = true;
			return numCollected < amountNeeded;
		//}
		
		//return false;
	}

	
	public boolean continueExecuting(){
		if(numCollected >= amountNeeded){
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}		
		
		if(System.currentTimeMillis() > startTime+timeOut){
			this.entity.currentTask.status = TaskStatus.FAILED;
			System.out.println("Didnt find wood");
			return false;
		}
		
		boolean foundWood = true;
		
		
		if(this.entity.getNavigator().noPath()){
			foundWood = findWood();
		}
		
		if(!foundWood || this.entity.getNavigator().noPath()){
			Vec3 vec = RandomPositionGenerator.findRandomTarget(entity, 20, 4);
			entity.getNavigator().tryMoveToXYZ(vec.xCoord, vec.yCoord, vec.zCoord, speed);
			return true;
		}
		
		collectWood();
		return true;
	}
	
	public void startExecuting(){
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		startTime = System.currentTimeMillis();
		findWood();
		collectWood();
		System.out.println("How often does this happen?");
		//woodX = pos.xCoord;
		//woodY = pos.yCoord;
		//woodZ = pos.zCoord;
	}
	
	//TODO: Make the bot search for wood if it cannot find any
	private boolean findWood(){
		int posx = MathHelper.floor_double(entity.posX - 10);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 10);
		
		boolean foundWood = searchLoop(posx, posy, posz);;
		
		
		if(foundWood){
			Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(entity.posX),
					MathHelper.floor_double(entity.posY+entity.getEyeHeight()),
					MathHelper.floor_double(entity.posZ));
			Vec3 end = world.getWorldVec3Pool().getVecFromPool(woodX, woodY, woodZ);
			
			MovingObjectPosition hit = world.clip(start, end);
			
			if(hit == null || (hit.blockX == end.xCoord && hit.blockY == end.yCoord && hit.blockZ == end.zCoord)){ //TODO: Might be blocked  by target block. Maybe check for hit position vs target position
				this.entity.getNavigator().tryMoveToXYZ(woodX, woodY, woodZ, speed);
			}
		}
		return foundWood;
		
	}
	
	private boolean searchLoop(int posx, int posy, int posz){
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 20; x++){
				for(int z = 0; z < 20; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.wood.blockID){
						woodX = posx+x;
						woodY = posy+y;
						woodZ = posz+z;
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private void collectWood(){
		int posx = MathHelper.floor_double(entity.posX -1);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 1);
		
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 3; x++){
				for(int z = 0; z < 3; z++){
					if(world.getBlockId(posx+x, posy+y, posz+z) == Block.leaves.blockID)
						world.setBlock(posx+x, posy+y, posz+z, 0);
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.wood.blockID){
						ItemStack stack = new ItemStack(Block.wood);
						entity.inventory.addItems(stack);
						numCollected++;
						world.setBlock(posx+x, posy+y, posz+z, 0);
					}
				}
			}
		}
	}

}
