package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAICollectIron extends EntityAIBase{
	private TheWallsBot entity;
	private World world;		
	double ironX = 0;
	double ironY = 0;
	double ironZ = 0;	
	double speed = 0.47;
	private int numCollected = 0;
	private int amountNeeded;
	private long timeOut = 4500;//ms
	private long startTime= 0;
	private boolean lock;
	
	public EntityAICollectIron(EntityLiving entity, int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.setMutexBits(1);
		this.amountNeeded = amount;
		lock = false;
	}
	
	@Override
	public boolean shouldExecute() {
		if(numCollected < amountNeeded && (entity.inventory.getItemCount(new ItemStack(Item.pickaxeStone))>0 || entity.inventory.getItemCount(new ItemStack(Item.pickaxeIron))>0 || entity.inventory.getItemCount(new ItemStack(Item.pickaxeDiamond))>0)){
			return true;
		}
		this.entity.currentTask.status = TaskStatus.FAILED;
		return false;
	}
	
	//TODO: 
	public boolean continueExecuting(){
		if(numCollected >= amountNeeded){
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}		
		
		if(System.currentTimeMillis() > startTime+timeOut){
			this.entity.currentTask.status = TaskStatus.FAILED;
			System.out.println("Didnt find iron");
			return false;
		}
		
		boolean foundIron = true;
		
		
		if(this.entity.getNavigator().noPath()){
			foundIron = findIron();
		}
		
		if(!foundIron || this.entity.getNavigator().noPath()){
			Vec3 vec = RandomPositionGenerator.findRandomTarget(entity, 20, 4);
			entity.getNavigator().tryMoveToXYZ(vec.xCoord, vec.yCoord, vec.zCoord, speed);
			return true;
		}
		
		collectIron();
		return true;
			
	}
	
	public void startExecuting(){
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		startTime = System.currentTimeMillis();
		findIron();
		collectIron();
		System.out.println("How often does this happen?");
		
	}
	
	private boolean findIron(){
		int posx = MathHelper.floor_double(entity.posX - 10);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 10);
		
		boolean foundIron = searchLoop(posx, posy, posz);;
		
		
		if(foundIron){
			Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(entity.posX),
					MathHelper.floor_double(entity.posY+entity.getEyeHeight()),
					MathHelper.floor_double(entity.posZ));
			Vec3 end = world.getWorldVec3Pool().getVecFromPool(ironX, ironY, ironZ);
			
			MovingObjectPosition hit = world.clip(start, end);
			
			if(hit == null || (hit.blockX == end.xCoord && hit.blockY == end.yCoord && hit.blockZ == end.zCoord)){ //TODO: Might be blocked  by target block. Maybe check for hit position vs target position
				this.entity.getNavigator().tryMoveToXYZ(ironX, ironY, ironZ, speed);
			}
			
			
		}
		return foundIron;
	}

	private void collectIron(){
		int posx = MathHelper.floor_double(entity.posX -1);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 1);
		
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 3; x++){
				for(int z = 0; z < 3; z++){
					if(world.getBlockId(posx+x, posy+y, posz+z) == Block.leaves.blockID)
						world.setBlock(posx+x, posy+y, posz+z, 0);
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.oreIron.blockID){
						ItemStack stack = new ItemStack(Block.oreIron);
						entity.inventory.addItems(stack);
						numCollected++;
						world.setBlock(posx+x, posy+y, posz+z, 0);
					}
				}
			}
		}
	}
	
	private boolean searchLoop(int posx, int posy, int posz){
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 20; x++){
				for(int z = 0; z < 20; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.oreIron.blockID){
						ironX = posx+x;
						ironY = posy+y;
						ironZ = posz+z;
						return true;
					}
				}
			}
		}
		return false;
		}
	
	
	
}
