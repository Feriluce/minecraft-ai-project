package minecraftBot.entity.ai;

import org.apache.commons.lang3.StringUtils;

import minecraftBot.entity.TheWallsBot;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EntityOwnable;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class EntityAINearestTargetSelector implements IEntitySelector{

	
    private  EntityAIBase entityAI;
    private TheWallsBot entityBot;
    private EntityLivingBase entityToAttack;

	public EntityAINearestTargetSelector(EntityAIBase entity, TheWallsBot entityBot)
	    {
	        this.entityAI = entity;
	        this.entityBot=entityBot;
	        this.entityToAttack=entityToAttack;
	    }

	    
    /**
     * Return whether the specified entity is applicable to this filter.
     */
    public boolean isEntityApplicable(Entity par1Entity)
    {
    	if(!(par1Entity instanceof EntityLivingBase)){
    		return false;
    	}else{
    		return this.isSuitableTarget(entityBot, par1Entity, true);
    	}
    	
        
    }

    
    
    
    public boolean isSuitableTarget(TheWallsBot entityBot, Entity entityToAttack, boolean par2){
    	if (entityToAttack == null)
        {
            return false;
        }
        else if (entityToAttack == entityBot)
        {
            return false;
        }
        else if (!entityToAttack.isEntityAlive())
        {
            return false;
        }
        else if (!entityBot.canAttackClass(entityToAttack.getClass()))
        {
            return false;
        }
        else
        {
            if (entityBot instanceof EntityOwnable && StringUtils.isNotEmpty(((EntityOwnable)entityBot).getOwnerName()))
            {
                if (entityToAttack instanceof EntityOwnable && ((EntityOwnable)entityBot).getOwnerName().equals(((EntityOwnable)entityToAttack).getOwnerName()))
                {
                    return false;
                }

                if (entityToAttack == ((EntityOwnable)entityBot).getOwner())
                {
                    return false;
                }
            }
            else if (entityToAttack instanceof EntityPlayer && !par2 && ((EntityPlayer)entityToAttack).capabilities.disableDamage)
            {
                return false;
            }

            if (!entityBot.func_110176_b(MathHelper.floor_double(entityToAttack.posX), MathHelper.floor_double(entityToAttack.posY), MathHelper.floor_double(entityToAttack.posZ)))
            {
                return false;
            }
            else if (!entityBot.getEntitySenses().canSee(entityToAttack))
            {
                return false;
            }
                return true;
            }
        }
    }

  

   

