package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAICollectChest extends EntityAIBase{

	
	private TheWallsBot entity;
	private World world;
	private int chestX = 0;
	private int chestY = 0;
	private int chestZ = 0;
	private boolean foundChest = false;
	private boolean collectedChest=false;
	private boolean working=false;
	
	public EntityAICollectChest(EntityLiving entity){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		
		//this.setMutexBits(1);
	}
	
	@Override
	public boolean shouldExecute() {
		findChest();
		if(foundChest && !working ){
			return true;
		}else{
			//System.out.println("No chests around.");
			return false;
		}
		
	}
	
	
	public boolean continueExecuting(){
		if(!collectedChest && !this.entity.getNavigator().noPath()){
		//findChest();
		collectChest();
		return true;
		} else if(collectedChest){
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}
		else {
			this.entity.currentTask.status = TaskStatus.FAILED;
			return false;
		}
	}
	
	public void startExecuting(){
		working=true;
		System.out.println("Starting executing ");
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		//findChest();
		collectChest();
		
	}
	
	
	private void findChest(){
		int posx = MathHelper.floor_double(entity.posX - 10);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 10);
		
		searchLoop(posx, posy, posz);
		
		if(foundChest){
			Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(entity.posX),
					MathHelper.floor_double(entity.posY),//+entity.getEyeHeight()),
					MathHelper.floor_double(entity.posZ));
			Vec3 end = world.getWorldVec3Pool().getVecFromPool(chestX, chestY, chestZ);
			
			if(world.clip(start, end) == null){ //TODO: Might be blocked  by target block. Maybe check for hit position vs target position
				this.entity.getNavigator().tryMoveToXYZ(chestX, chestY, chestZ, 0.5D);
			}
		}
	}
	
	private void searchLoop(int posx, int posy, int posz){
		
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 40; x++){
				for(int z = 0; z <40; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.chest.blockID){
						chestX = posx+x;
						chestY = posy+y;
						chestZ = posz+z;
						foundChest = true;
						return;
					}
				}
			}
		}
		
	}
	
	private void collectChest(){
		int posx = MathHelper.floor_double(entity.posX -1);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 1));
		int posz = MathHelper.floor_double(entity.posZ - 1);
		
		/*
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 3; x++){
				for(int z = 0; z < 3; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.chest.blockID){
						addChestToInventory();
						
						world.setBlock(posx+x, posy+y, posz+z, 0);
						
					}
				}
			}
		}*/
		
		addChestToInventory();
		
		//world.setBlock(chestX, chestY, chestZ, 0);
		collectedChest=true;

		
		
	}
	
	private void addChestToInventory(){
		System.out.println("DoesBlockHaveTileEntity: " + world.blockHasTileEntity(chestX, chestY, chestZ));
		System.out.println("Block in pos "+chestX+","+chestY+","+chestZ+" is of type: "+ world.getBlockTileEntity(chestX, chestY, chestZ).blockType);
		//if(world.getBlockTileEntity(chestX, chestY, chestZ).blockType instanceof BlockChest){
		BlockChest blockChest=  (BlockChest) world.getBlockTileEntity(chestX, chestY, chestZ).blockType;
		
		System.out.println("blockchest: "+blockChest.toString());
		IInventory chestInventory= blockChest.getInventory(world, chestX, chestY, chestZ);
		for(ItemStack i: entity.inventory.getItemStackList()){
			System.out.println(i.toString()+ "size: " + i.stackSize);
		}
		
		
		for(int i=0; i< chestInventory.getSizeInventory();i++){
			ItemStack is= chestInventory.decrStackSize(i,64);
			entity.inventory.addItems(is);
		}
		
		ItemStack is= chestInventory.decrStackSize(0, chestInventory.getSizeInventory());
		entity.inventory.addItems(is);
		System.out.println(is.itemID);
		for(ItemStack i: entity.inventory.getItemStackList()){
			System.out.println(i.toString()+ "size: " + i.stackSize);
		}
		//}
		world.setBlock(chestX, chestY, chestZ, 0);
		
		//TODO: understand how to retrieve the tools/block inside the chest
		//ItemStack stack = new ItemStack(Block.chest);	
		//entity.inventory.addItems(stack);
	}
}
