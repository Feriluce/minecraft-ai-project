package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAIExplore extends EntityAIBase{

	
	private static double SPEED_MOVEAROUND=0.4;
	private static long MAXTIME=15000; //ms
	private long startingTime=0;
	private TheWallsBot entity;
	private World world;
	private double rndPosX=0;
	private double rndPosY=0;
	private double rndPosZ=0;
	private boolean firstTime=true;
	
	
	/**
	 * random direction for digging
	 */
	private String direction= null;
	
	/**
	 * true if the bot is moving around, false if it's digging
	 */
	private boolean flag=true;

	
	public EntityAIExplore (EntityLiving entity){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;	
		this.setMutexBits(1);
	}

	@Override
	public boolean shouldExecute() {		
		return true;		
	}
	
	
	public void startExecuting(){
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		this.startingTime=System.currentTimeMillis();
		flag=true;
		this.moveAround();	
	}
	
	//it's never returning false
	public boolean continueExecuting(){
			if(flag){
				if(System.currentTimeMillis()- startingTime <MAXTIME){
					moveAround();						
				}else{
					flag=false;
					this.selectDirection();
					dig();						
				}
			}else{
				dig();
				}
			return true;
			}			
	
	private void dig() {
		System.out.println("digging");
		int Xpos1=0, Zpos1=0, Ypos1=0, Xpos2=0, Zpos2=0, Ypos2=0, Xpos3=0, Zpos3=0, Ypos3=0;
		
		System.out.println("direction: "+ direction);
		
		if(direction=="x"){ //&& (entity.getNavigator().tryMoveToXYZ(entity.posX+3, entity.posY, entity.posZ, this.SPEED_MOVEAROUND) || firstTime)){
			Xpos1 = MathHelper.floor_double(entity.posX +1);
			Zpos1 = MathHelper.floor_double(entity.posZ);
			Ypos1 = MathHelper.floor_double(entity.posY - 1);

			Xpos2 = MathHelper.floor_double(entity.posX +2);
			Zpos2 = MathHelper.floor_double(entity.posZ);
			Ypos2 = MathHelper.floor_double(entity.posY - 1);

			Xpos3 = MathHelper.floor_double(entity.posX +2);
			Zpos3 = MathHelper.floor_double(entity.posZ);
			Ypos3 = MathHelper.floor_double(entity.posY);
		}
		
		if(direction=="-x"){// && (entity.getNavigator().tryMoveToXYZ(entity.posX-3, entity.posY, entity.posZ, this.SPEED_MOVEAROUND) || firstTime)){
			Xpos1 = MathHelper.floor_double(entity.posX -1);
			Zpos1 = MathHelper.floor_double(entity.posZ);
			Ypos1 = MathHelper.floor_double(entity.posY - 1);
			
			Xpos2 = MathHelper.floor_double(entity.posX -2);
			Zpos2 = MathHelper.floor_double(entity.posZ);
			Ypos2 = MathHelper.floor_double(entity.posY - 1);
			
			Xpos3 = MathHelper.floor_double(entity.posX -2);
			Zpos3 = MathHelper.floor_double(entity.posZ);
			Ypos3 = MathHelper.floor_double(entity.posY);			
		}
		
		if(direction=="z"){//  && (entity.getNavigator().tryMoveToXYZ(entity.posX, entity.posY, entity.posZ+3, this.SPEED_MOVEAROUND) || firstTime)){
			Xpos1 = MathHelper.floor_double(entity.posX);
			Zpos1 = MathHelper.floor_double(entity.posZ + 1);
			Ypos1 = MathHelper.floor_double(entity.posY - 1);

			Xpos2 = MathHelper.floor_double(entity.posX);
			Zpos2 = MathHelper.floor_double(entity.posZ +2);
			Ypos2 = MathHelper.floor_double(entity.posY - 1);
			
			Xpos3 = MathHelper.floor_double(entity.posX);
			Zpos3 = MathHelper.floor_double(entity.posZ +2);
			Ypos3 = MathHelper.floor_double(entity.posY);			
		}
		
		if(direction=="-z" ){// && (entity.getNavigator().tryMoveToXYZ(entity.posX, entity.posY, entity.posZ-3, this.SPEED_MOVEAROUND) || firstTime)){
			Xpos1 = MathHelper.floor_double(entity.posX);
			Zpos1 = MathHelper.floor_double(entity.posZ - 1);
			Ypos1 = MathHelper.floor_double(entity.posY - 1);

			Xpos2 = MathHelper.floor_double(entity.posX);
			Zpos2 = MathHelper.floor_double(entity.posZ -2);
			Ypos2 = MathHelper.floor_double(entity.posY - 1);
			
			Xpos3 = MathHelper.floor_double(entity.posX);
			Zpos3 = MathHelper.floor_double(entity.posZ - 2);
			Ypos3 = MathHelper.floor_double(entity.posY);			
		}
		
		//firstTime=false;
		if(canDig(world.getBlockId((int)Xpos1, (int)Zpos1, (int)Ypos1))){
			digBlock((int)Xpos1, (int)Ypos1, (int)Zpos1);
		}
		/*else {
			selectDirection();
		}*/
		
		if(canDig(world.getBlockId((int)Xpos2, (int)Zpos2, (int)Ypos2))){
			digBlock((int)Xpos2, (int)Ypos2, (int)Zpos2);
		}/*else {
			selectDirection();
		//}*/
		
		if(canDig(world.getBlockId((int)Xpos3, (int)Zpos3, (int)Ypos3))){
			digBlock((int)Xpos3, (int)Ypos3, (int)Zpos3);
		}/*else {
			selectDirection();
		}*/
		
			moveAfterDig();
		//if(entity.getNavigator().tryMoveToXYZ(entity.posX, entity.posY, entity.posZ-3, this.SPEED_MOVEAROUND)){
			//flag=true;
		//}
	}
		
	private void moveAround(){
		nextRandomMove();
        this.entity.getNavigator().tryMoveToXYZ(this.rndPosX, this.rndPosY, this.rndPosZ, this.SPEED_MOVEAROUND);
	}
		
	public boolean canDig(int blockID){
		if (blockID != Block.bedrock.blockID && blockID != Block.sand.blockID){
			return true;
		}else return false;
	}
	
	private void nextRandomMove(){
		Vec3 vec3 = RandomPositionGenerator.findRandomTarget(this.entity, 10, 5);
       if(vec3!=null){   	   
    	   this.rndPosX = vec3.xCoord;
           this.rndPosY = vec3.yCoord;
           this.rndPosZ = vec3.zCoord;
       }           
     }
	
	
	private void digBlock(int x, int y, int z){
		//wait some time to make it more real
		System.out.println("destroy");
		System.out.println(world.getBlockId(x, y, z));
		long time= System.currentTimeMillis();
		do{}while(System.currentTimeMillis()-time<100);
		//world.setBlock(x, y, z, 0);
		world.destroyBlock(x, y, z, true);
	}
	
	private void selectDirection(){
		int rnd= (int) (Math.random()*3);
		switch(rnd){
			case 0:
				direction="x";
				break;
			case 1:
				direction= "-x";
				break;
			case 2:
				direction="z";
				break;
			case 3:
				direction="-z";
				break;
			default:
				direction=null;
		}				
	}
	
	private void moveAfterDig(){
		if(direction=="x"){
			this.entity.getNavigator().tryMoveToXYZ(entity.posX+1, entity.posY-1, entity.posZ, this.SPEED_MOVEAROUND);			
		}
		if(direction=="-x"){
			this.entity.getNavigator().tryMoveToXYZ(entity.posX-1, entity.posY-1, entity.posZ, this.SPEED_MOVEAROUND);			
		}
		if(direction=="z"){
			this.entity.getNavigator().tryMoveToXYZ(entity.posX, entity.posY-1, entity.posZ+1, this.SPEED_MOVEAROUND);						
		}
		if(direction=="-z"){
			this.entity.getNavigator().tryMoveToXYZ(entity.posX, entity.posY-1, entity.posZ-1, this.SPEED_MOVEAROUND);						
		}
		
	}
	
	
}
