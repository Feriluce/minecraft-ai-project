package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityAIDigDown extends EntityAIBase{
	
	private TheWallsBot entity;
	private World world;
	private int amount;
	private int blockToDigID=-1;
	private int Xpos=0;
	private int Ypos=0;
	private int Zpos=0;
	private int digCount=0;
	boolean digged=false;
	
	
	
	
	public EntityAIDigDown(EntityLiving entity, int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.amount=0;
		this.blockToDigID=findBlockToDig();
		//TODO:mutexBit
	}

	@Override
	public boolean shouldExecute() {
		//also water or lava,ecc...????
		if (blockToDigID != Block.bedrock.blockID && blockToDigID != Block.sand.blockID && blockToDigID!=0){
			return true;
		}
		System.out.println("You cannot dig this block.");
		this.entity.currentTask.status = TaskStatus.FAILED;
		return false;
		
	}
	
	
	public void startExecuting(){
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		this.blockToDigID=findBlockToDig();
		dig();
	}
	
	public boolean continueExecuting(){
		if(!digged){
			this.blockToDigID=findBlockToDig();
			dig();
			return true;
		}else{
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}		
	}
	
	
	
	private void dig() {
		//wait some time
		long time= System.currentTimeMillis();
		do{}while(System.currentTimeMillis()-time<100);
		
		world.destroyBlock(Xpos, Ypos, Zpos, true);
		digged=true;
		digCount++;
		System.out.println("The block in front of the bot has been digged.");
	}
	
	
	/**method used to find the block in front of the entity.
	 * @return
	 */
	private int findBlockToDig(){
		Xpos =  MathHelper.floor_double(entity.posX +(entity.posX-entity.prevPosX));
		Zpos = MathHelper.floor_double(entity.posZ + (entity.posX-entity.prevPosX));
		/*for(int i=1; i<= amount; i++){
			Ypos = MathHelper.floor_double(entity.posY - i);
			if(world.getBlockId(Xpos, Ypos, Zpos)!= 0){
				break;
			}
		}*/
		Ypos = MathHelper.floor_double(entity.posY - 1);
		return world.getBlockId(Xpos, Ypos, Zpos);
	}

}
