package minecraftBot.entity.ai;

import java.util.ArrayList;

import minecraftBot.entity.TheWallsBot;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAIHuntPlayers extends EntityAIBase {
	TheWallsBot entity;
	EntityPlayer target;
	World world;
	double speed = 0.47;
	
	EntityAIHuntPlayers(TheWallsBot entity){
		this.entity = entity;
		this.world = entity.worldObj;
	}

	@Override
	public boolean shouldExecute() {
		target = findClosestPlayer();
		return target != null;
	}
	@Override
	public boolean continueExecuting(){
		if(target!=null && target.isDead)
			target = findClosestPlayer();
		if(target == null)
			target = findClosestPlayer();
		
		if(entity.getNavigator().noPath())
			huntPlayer();
		
		return true;
	}
	
	@Override
	public void startExecuting(){
		huntPlayer();
	}
	
	 private EntityPlayer findClosestPlayer(){		
		ArrayList<EntityPlayer> players = (ArrayList<EntityPlayer>) world.playerEntities;
		 EntityPlayer closestPlayer=null;
		 
		 
		 
		 float minDistance=Float.MAX_VALUE;
		 if(players.size()>0){
			 for(EntityPlayer p: players){			
				 if(this.entity.getSpawningPlayer() == null){
					 if(entity.getDistanceToEntity(p)<minDistance){
						 closestPlayer=p;
						 minDistance=entity.getDistanceToEntity(p);
					 }					 
					 continue;
				 }
				 if(entity.getDistanceToEntity(p)<minDistance && ( p.getTeam()!=this.entity.getSpawningPlayer().getTeam() )){
					 closestPlayer=p;
					 minDistance=entity.getDistanceToEntity(p);
				 }
			 }
		 }
		 return closestPlayer;
	 }
	 
	 private void huntPlayer(){
		 Vec3 start = world.getWorldVec3Pool().getVecFromPool(
					MathHelper.floor_double(entity.posX),
					MathHelper.floor_double(entity.posY+entity.getEyeHeight()),
					MathHelper.floor_double(entity.posZ));
		Vec3 end = world.getWorldVec3Pool().getVecFromPool(target.posX, target.posY, target.lastTickPosZ);
		MovingObjectPosition hit = world.clip(start, end);
		if(target != null && (hit == null || (hit.blockX == end.xCoord && hit.blockY == end.yCoord && hit.blockZ == end.zCoord))){ 
			this.entity.getNavigator().tryMoveToXYZ(target.posX, target.posY, target.posZ, speed);
		} else {
			Vec3 vec = RandomPositionGenerator.findRandomTarget(entity, 20, 4);
			entity.getNavigator().tryMoveToXYZ(vec.xCoord, vec.yCoord, vec.zCoord, speed);
		}
	 }
	 

}
