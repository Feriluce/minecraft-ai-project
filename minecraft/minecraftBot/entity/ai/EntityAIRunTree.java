package minecraftBot.entity.ai;

import minecraftBot.entity.TheWallsBot;
import minecraftBot.exceptions.NoReadyException;
import net.minecraft.entity.ai.EntityAIBase;

public class EntityAIRunTree extends EntityAIBase {
	TheWallsBot bot;
	
	public EntityAIRunTree(TheWallsBot bot){
		this.bot = bot;
	}
	
	@Override
	public boolean shouldExecute() {
		return true;
	}

	@Override
	public boolean continueExecuting(){
		try {
			bot.mainTree.root.execute();
		} catch (NoReadyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bot.mainTree.resetTree();
		return true;
	}
	
	@Override
	public void startExecuting(){
		try {
			bot.mainTree.root.execute();
		} catch (NoReadyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bot.mainTree.resetTree();
	}
}
