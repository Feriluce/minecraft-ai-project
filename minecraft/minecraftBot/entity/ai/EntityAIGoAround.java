package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAIGoAround extends EntityAIBase{

	private TheWallsBot entity;
	private World world;
	private double xPosition;
    private double yPosition;
    private double zPosition; 
    private double speed;

    
	// limit in the number of seconds to go around
	private float timeAround;
	private float startingTime=0;
	
	public EntityAIGoAround(EntityLiving entity, int maxTime, double speed){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.timeAround=maxTime;
		this.speed=speed;
		this.setMutexBits(1);
		
		//TODO:check for the mutexbit
	}
	
	
	@Override
	public boolean shouldExecute() {	
		Vec3 vec3 = RandomPositionGenerator.findRandomTarget(this.entity, 10, 7);
        if (vec3 == null)
        {
        	entity.currentTask.status = TaskStatus.FAILED;
            return false;
        }
        else
        {
            this.xPosition = vec3.xCoord;
            this.yPosition = vec3.yCoord;
            this.zPosition = vec3.zCoord;
            return true;
        }
	}

	
	public void startExecuting(){
		entity.currentTask.task = this;
		entity.currentTask.status = TaskStatus.RUNNING;
		this.startingTime=(int) System.currentTimeMillis();
		goAround();
		
	}
	
	public boolean continueExecuting(){
		if((System.currentTimeMillis()- startingTime) <=timeAround && !this.entity.getNavigator().noPath()){
			goAround();		
			return true;			
		} else {
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}
		
	}
	
	private void goAround(){		
        this.entity.getNavigator().tryMoveToXYZ(this.xPosition, this.yPosition, this.zPosition, this.speed);
		}
	
}
