package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityAICreatePlanks extends EntityAIBase{

	private TheWallsBot entity;
	private World world;
	private int amount;
	private int numCreated=0;
	
	public EntityAICreatePlanks(EntityLiving entity, int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.amount=amount;

	}
	
	
	@Override
	public boolean shouldExecute() {
		if(entity.inventory.getItemCount(new ItemStack(Block.wood))>=1*(amount/4 +1)){
			return true;
		}
		System.out.println("There aren't enough raw materials to create "+amount+" plank.");
		entity.currentTask.status = TaskStatus.FAILED;
		return false;
	}
	
	
	public boolean continueExecuting(){
		if(numCreated<amount){
			createPlanks();
			return true;
		} else {
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}
	}
	
	private void createPlanks(){	
		System.out.println("Starting planks creation");
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.planks))+"blocks of "+ Block.planks.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.wood))+"blocks of "+ Block.wood.toString());	
		entity.inventory.removeItems(new ItemStack(Block.wood, 1));
		entity.inventory.addItems(new ItemStack(Block.planks, 4));
		numCreated=numCreated+4;
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.planks))+"blocks of "+ Block.planks.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.wood))+"blocks of "+ Block.wood.toString());	
				
	}
	
	public void startExecuting(){
		if(shouldExecute()){
			entity.currentTask.task = this;
			entity.currentTask.status = TaskStatus.RUNNING;
			createPlanks();
		}		
	}
}
