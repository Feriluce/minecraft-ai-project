package minecraftBot.entity.ai;

import minecraftBot.entity.TaskStatus;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityAICreateStick extends EntityAIBase{

	private TheWallsBot entity;
	private World world;
	private int amount;
	private int numCreated=0;
	
	public EntityAICreateStick(EntityLiving entity, int amount){
		this.entity = (TheWallsBot)entity;
		this.world = entity.worldObj;
		this.amount=amount;
	}
	
	
	@Override
	public boolean shouldExecute() {
		if((entity.inventory.getItemCount(new ItemStack(Block.wood))>=1*(amount/8 +1) || entity.inventory.getItemCount(new ItemStack(Block.planks))>=2*(amount/4 +1))){
			return true;
		}
		System.out.println("There aren't the raw materials to create "+ amount+" stick.");
		entity.currentTask.status = TaskStatus.FAILED;
		return false;
	}
	
	public boolean continueExecuting(){
		if(numCreated<amount){
			createSticks();
			return true;
		} else {
			this.entity.currentTask.status = TaskStatus.SUCCEEDED;
			return false;
		}
	}
	
	private void createSticks(){	
		System.out.println("Starting stick creation");
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.planks))+"blocks of "+ Block.planks.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.wood))+"blocks of "+ Block.wood.toString());
		System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Item.stick))+"of "+ Item.stick.toString());		
		
			if(entity.inventory.getItemCount(new ItemStack(Block.planks))<2){
				entity.inventory.removeItems(new ItemStack(Block.wood, 1));
				entity.inventory.addItems(new ItemStack(Item.stick, 8));
				numCreated=numCreated+8;
			}else {
				entity.inventory.removeItems(new ItemStack(Block.planks, 2));
				entity.inventory.addItems(new ItemStack(Item.stick, 4));
				numCreated=numCreated+8;
			}				
			System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Item.stick))+"of "+ Item.stick.toString());		
			System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.planks))+"blocks of "+ Block.planks.toString());
			System.out.println("The inventory contains "+ entity.inventory.getItemCount(new ItemStack(Block.wood))+"blocks of "+ Block.wood.toString());
			
	}
	
	public void startExecuting(){
		if(shouldExecute()){
			entity.currentTask.task = this;
			entity.currentTask.status = TaskStatus.RUNNING;
			createSticks();
		}		
	}

}
