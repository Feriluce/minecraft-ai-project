package minecraftBot.entity;

import net.minecraft.entity.ai.EntityAIBase;

public class RunningTask {
	public EntityAIBase task;
	public TaskStatus status;
	
	public RunningTask(){
		this.task = null;
		this.status = TaskStatus.NONE;
	}
}
