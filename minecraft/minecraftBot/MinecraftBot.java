package minecraftBot;

import minecraftBot.commands.CommandEvolve;
import minecraftBot.commands.CommandSpawnWallBot;
import minecraftBot.entity.TheWallsBot;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityEggInfo;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.biome.BiomeGenBase;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
 
@Mod(modid="MinecraftBot", name="MinecraftBot", version="0.0.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false)

public class MinecraftBot {
 
    // The instance of your mod that Forge uses.
    @Instance("MinecraftBot")
    public static MinecraftBot instance;
   
    // Says where the client and server 'proxy' code is loaded.
    @SidedProxy(clientSide="minecraftBot.client.ClientProxy", serverSide="minecraftBot.CommonProxy")
    public static CommonProxy proxy;
   
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
            // Stub Method
    }
   
    @EventHandler
    public void load(FMLInitializationEvent event) {
            proxy.registerRenderers();
            registerEntity(TheWallsBot.class, "TheWallsBot", 0xEAEAE9, 0xC99A03);
            LanguageRegistry.instance().addStringLocalization("entity.TheWallsBot.name", "The Walls Bot");
    }
   
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        // Stub Method
    }
        
    @EventHandler
    public void serverStart(FMLServerStartingEvent event){
    	MinecraftServer server = MinecraftServer.getServer();
    	ICommandManager command = server.getCommandManager();
    	ServerCommandManager manager = (ServerCommandManager) command;
    	
    	manager.registerCommand(new CommandEvolve());
    	manager.registerCommand(new CommandSpawnWallBot());
    }
    
    public void registerEntity(Class<? extends Entity> entityClass, String entityName, int bkEggColor, int fgEggColor) {
        int id = EntityRegistry.findGlobalUniqueEntityId();

        EntityRegistry.registerGlobalEntityID(entityClass, entityName, id);
        EntityList.entityEggs.put(Integer.valueOf(id), new EntityEggInfo(id, bkEggColor, fgEggColor));
    }

    public void addSpawn(Class<? extends EntityLiving> entityClass, int spawnProb, int min, int max, BiomeGenBase[] biomes) {
        if (spawnProb > 0) {
                EntityRegistry.addSpawn(entityClass, spawnProb, min, max, EnumCreatureType.creature, biomes);
        }
    }
}
