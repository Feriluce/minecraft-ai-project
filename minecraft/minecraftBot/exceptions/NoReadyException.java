package minecraftBot.exceptions;

public class NoReadyException extends Exception {
	
	public NoReadyException(){
		super ("The node isn't ready!");
	}

}
