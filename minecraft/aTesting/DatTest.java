package aTesting;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityEggInfo;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid="Test1", name="DatTest", version="0.1")
@NetworkMod(clientSideRequired=true)

public class DatTest {
	
	public static final Block superDirt = new SuperBlock(500, Material.ground).setHardness(0.5f).
			setStepSound(Block.soundGrassFootstep).setUnlocalizedName("SuperBlock").setCreativeTab(CreativeTabs.tabBlock);
	
	@Instance(value="Test1")
	public static DatTest instance;
	
	@SidedProxy(clientSide="aTesting.client.ClientProxy", serverSide="aTesting.CommonProxy")
    public static CommonProxy proxy;
	
	@EventHandler // used in 1.6.2
    //@PreInit    // used in 1.5.2
    public void preInit(FMLPreInitializationEvent event) {
            // Stub Method
    }
   
    @EventHandler // used in 1.6.2
    //@Init       // used in 1.5.2
    public void load(FMLInitializationEvent event) {
            proxy.registerRenderers();
            ItemStack dirtStack = new ItemStack(Block.dirt);
            ItemStack diamondStack = new ItemStack(Item.diamond, 64);
            ItemStack blackwool = new ItemStack(Block.cloth, 10, 15);
            ItemStack stoneStack = new ItemStack(1, 32, 0);
            
            GameRegistry.addShapelessRecipe(diamondStack, dirtStack);
            GameRegistry.addShapelessRecipe(diamondStack, dirtStack, diamondStack);
            
            GameRegistry.addRecipe(diamondStack, "x", "x",
            		'x', dirtStack);
            
            
            GameRegistry.addSmelting(Block.dirt.blockID, diamondStack, 10000f);
            
            GameRegistry.registerBlock(superDirt, "SuperBlock");
            LanguageRegistry.addName(superDirt, "Super Dirt");
            MinecraftForge.setBlockHarvestLevel(superDirt, "shovel", 2	);
            
            
            registerEntity(RetardZombie.class, "RetardZombie", 0xEAEAE9, 0xC99A03);
            LanguageRegistry.instance().addStringLocalization("entity.RetardZombie.name", "Retard Zombie");
    }
   
    @EventHandler // used in 1.6.2
    //@PostInit   // used in 1.5.2
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }	
    
    
    public void registerEntity(Class<? extends Entity> entityClass, String entityName, int bkEggColor, int fgEggColor) {
        int id = EntityRegistry.findGlobalUniqueEntityId();

        EntityRegistry.registerGlobalEntityID(entityClass, entityName, id);
        EntityList.entityEggs.put(Integer.valueOf(id), new EntityEggInfo(id, bkEggColor, fgEggColor));
}

    public void addSpawn(Class<? extends EntityLiving> entityClass, int spawnProb, int min, int max, BiomeGenBase[] biomes) {
        if (spawnProb > 0) {
                EntityRegistry.addSpawn(entityClass, spawnProb, min, max, EnumCreatureType.creature, biomes);
        }
}
	
}
