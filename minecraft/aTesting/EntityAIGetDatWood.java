package aTesting;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityAIGetDatWood extends EntityAIBase{
	
	EntityCreature entity;
	World world;
	double woodX;
	double woodY;
	double woodZ;
	boolean foundWood = false;
	
	
	public EntityAIGetDatWood(EntityCreature datEntity){
		this.entity = datEntity;
		this.world = datEntity.worldObj;
		this.setMutexBits(1);
	}

	public boolean shouldExecute() {
		// TODO Auto-generated method stub
		return true;
	}
	
	public boolean continueExecuting(){
		return true;
	}
	
	public void startExecuting(){
		findWood();
		
		//woodX = pos.xCoord;
		//woodY = pos.yCoord;
		//woodZ = pos.zCoord;
	}
	
	private void findWood(){
		int posx = MathHelper.floor_double(entity.posX -5);
		int posy = MathHelper.floor_double((entity.boundingBox.minY - 2));
		int posz = MathHelper.floor_double(entity.posZ - 5);
		
		for(int y = 0; y < 4; y++){
			for(int x = 0; x <10; x++){
				for(int z = 0; z < 10; z++){
					if(world.getBlockId(posx + x, posy + y, posz + z) == Block.wood.blockID){
						world.setBlock(posx+x, posy+y, posz+z, 0);//return world.getWorldVec3Pool().getVecFromPool((double) (posx+x), (double) (posy+y), (double) (posz+z));
					}
				}
			}
		}
		//return null;
	}
	
}
